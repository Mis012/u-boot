/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2019
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 */

#ifndef __SANDBOX_CLK_H__
#define __SANDBOX_CLK_H__

#include <linux/clk-provider.h>
#include <semi-static-struct.h>

enum {
	SANDBOX_CLK_PLL2 = 1,
	SANDBOX_CLK_PLL3,
	SANDBOX_CLK_PLL3_60M,
	SANDBOX_CLK_PLL3_80M,
	SANDBOX_CLK_ECSPI_ROOT,
	SANDBOX_CLK_ECSPI0,
	SANDBOX_CLK_ECSPI1,
	SANDBOX_CLK_USDHC1_SEL,
	SANDBOX_CLK_USDHC2_SEL,
	SANDBOX_CLK_I2C,
	SANDBOX_CLK_I2C_ROOT,
};

enum sandbox_pllv3_type {
	SANDBOX_PLLV3_GENERIC,
	SANDBOX_PLLV3_USB,
};


#define sandbox_clk_pllv3(_x, _id, _type, _name, _parent, _base, _div_mask) \
	MAKE_SEMI_STATIC_STRUCT_5(struct clk_pllv3, _x, \
		div_mask, _div_mask, \
		clk.name, _name, \
		clk.id, _id, \
		clk.parent, _parent, \
		clk.ops, &sandbox_clk_pllv3_generic_ops \
	)

#define sandbox_clk_fixed_factor(_x, _id, _name, _parent, _mult, _div) \
	MAKE_SEMI_STATIC_STRUCT_7(struct clk_fixed_factor, _x, \
		mult, _mult, \
		div, _div, \
		clk.name, _name, \
		clk.id, _id, \
		clk.parent, _parent, \
		clk.flags, CLK_SET_RATE_PARENT, \
		clk.ops, &ccf_clk_fixed_factor_ops \
	)

#define sandbox_clk_divider(_x, _id, _name, _parent, _reg, _shift, _width) \
	MAKE_SEMI_STATIC_STRUCT_8(struct clk_divider, _x,\
		reg, (void *)_reg, \
		shift, _shift, \
		width, _width, \
		clk.name, _name, \
		clk.id, _id, \
		clk.parent, _parent, \
		clk.flags, CLK_SET_RATE_PARENT, \
		clk.ops, &clk_divider_ops \
	)

#define sandbox_clk_gate(_x, _id, _name, _parent, _reg, _shift, _flags) \
	MAKE_SEMI_STATIC_STRUCT_7(struct clk_gate, _x, \
		reg, (void *)_reg, \
		bit_idx, _shift, \
		clk.name, _name, \
		clk.id, _id, \
		clk.parent, _parent, \
		clk.flags, CLK_SET_RATE_PARENT | _flags, \
		clk.ops, &clk_gate_ops \
	)

#define sandbox_clk_gate2(_x, _id, _name, _parent, _reg, _shift) \
	MAKE_SEMI_STATIC_STRUCT_6(struct clk_gate2, _x, \
		state, 0, \
		clk.name, _name, \
		clk.id, _id, \
		clk.parent, _parent, \
		clk.flags, CLK_SET_RATE_PARENT, \
		clk.ops, &sandbox_clk_gate2_ops \
	)

#define sandbox_clk_mux(_x, _id, _name, _reg, _shift, _width, _parents, _num_parents) \
	MAKE_SEMI_STATIC_STRUCT_9(struct clk_mux, _x, \
		reg, (void *)_reg, \
		shift, _shift, \
		mask, BIT(_width) - 1, \
		parents, _parents, \
		num_parents, _num_parents, \
		clk.name, _name, \
		clk.id, _id, \
		clk.flags, CLK_SET_RATE_NO_REPARENT, \
		clk.ops, &clk_mux_ops \
	)

int sandbox_clk_enable_count(struct clk *clk);

#endif /* __SANDBOX_CLK_H__ */
