// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2019
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 */

#include <common.h>
#include <clk.h>
#include <dm.h>
#include <asm/clk.h>
#include <dm/test.h>
#include <dm/uclass.h>
#include <linux/err.h>
#include <test/test.h>
#include <test/ut.h>
#include <sandbox-clk.h>

/* we can't get the clocks from the device tree in a test driver, so we construct the fat pointer by hand */
static void clk_get_by_id(struct udevice *dev, int id, struct clk **_clk)
{
	struct clk *clk = kzalloc(sizeof(struct clk), GFP_KERNEL);
	clk->dev = dev;
	clk->id = id;

	*_clk = clk;
}

/* TODO: make this generally available? probably want to have the struct clk allocated by the user */
static struct clk *clk_ccf_get_parent(struct clk *clk)
{
	struct clk_core *clk_core = clk_get_core(clk);
	struct clk *pclk = kzalloc(sizeof(struct clk), GFP_KERNEL);
	pclk->dev = clk_core->parent->dev;
	pclk->id = clk_core->parent->id;
	return pclk;
}

/* Tests for Common Clock Framework driver */
static int dm_test_clk_ccf(struct unit_test_state *uts)
{
	struct clk *clk;
	struct clk *pclk;
	struct clk_core *clk_core;
	struct udevice *dev;
	long long rate;
	int ret;
#if CONFIG_IS_ENABLED(CLK_CCF)
	const char *clkname;
	int clkid, i;
#endif

	/* Get the device using the clk device */
	ut_assertok(uclass_get_device_by_name(UCLASS_CLK, "clk-ccf", &dev));

	/* Test for clk_get_by_id() */
	clk_get_by_id(dev, SANDBOX_CLK_ECSPI_ROOT, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("ecspi_root", clk_core->name);
	ut_asserteq(CLK_SET_RATE_PARENT, clk_core->flags);

	/* Test for clk_get_parent_rate() */
	clk_get_by_id(dev, SANDBOX_CLK_ECSPI1, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("ecspi1", clk_core->name);
	ut_asserteq(CLK_SET_RATE_PARENT, clk_core->flags);

	rate = clk_get_parent_rate(clk_core);
	ut_asserteq(rate, 20000000);

	/* test the gate of CCF */
	clk_get_by_id(dev, SANDBOX_CLK_ECSPI0, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("ecspi0", clk_core->name);
	ut_asserteq(CLK_SET_RATE_PARENT, clk_core->flags);

	rate = clk_get_parent_rate(clk_core);
	ut_asserteq(rate, 20000000);

	/* Test the mux of CCF */
	clk_get_by_id(dev, SANDBOX_CLK_USDHC1_SEL, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("usdhc1_sel", clk_core->name);
	ut_asserteq(CLK_SET_RATE_NO_REPARENT, clk_core->flags);

	rate = clk_get_parent_rate(clk_core);
	ut_asserteq(rate, 60000000);

	rate = clk_get_rate(clk);
	ut_asserteq(rate, 60000000);

	clk_get_by_id(dev, SANDBOX_CLK_PLL3_80M, &pclk);

	ret = clk_set_parent(clk, pclk);
	ut_assertok(ret);

	rate = clk_get_rate(clk);
	ut_asserteq(rate, 80000000);

	clk_get_by_id(dev, SANDBOX_CLK_USDHC2_SEL, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("usdhc2_sel", clk_core->name);
	ut_asserteq(CLK_SET_RATE_NO_REPARENT, clk_core->flags);

	rate = clk_get_parent_rate(clk_core);
	ut_asserteq(rate, 80000000);

	pclk = clk_ccf_get_parent(clk);
	clk_core = clk_get_core(pclk);
	ut_asserteq_str("pll3_80m", clk_core->name);
	ut_asserteq(CLK_SET_RATE_PARENT, clk_core->flags);

	rate = clk_get_rate(clk);
	ut_asserteq(rate, 80000000);

	clk_get_by_id(dev, SANDBOX_CLK_PLL3_60M, &pclk);

	ret = clk_set_parent(clk, pclk);
	ut_assertok(ret);

	rate = clk_get_rate(clk);
	ut_asserteq(rate, 60000000);

	/* Test the composite of CCF */
	clk_get_by_id(dev, SANDBOX_CLK_I2C, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("i2c", clk_core->name);
	ut_asserteq(CLK_SET_RATE_UNGATE, clk_core->flags);

	rate = clk_get_rate(clk);
	ut_asserteq(rate, 60000000);

#if CONFIG_IS_ENABLED(CLK_CCF)
	/* Test clk tree enable/disable */
	clk_get_by_id(dev, SANDBOX_CLK_I2C_ROOT, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("i2c_root", clk_core->name);

	ret = clk_enable(clk);
	ut_assertok(ret);

	ret = sandbox_clk_enable_count(clk);
	ut_asserteq(ret, 1);

	clk_get_by_id(dev, SANDBOX_CLK_I2C, &pclk);

	ret = sandbox_clk_enable_count(pclk);
	ut_asserteq(ret, 1);

	ret = clk_disable(clk);
	ut_assertok(ret);

	ret = sandbox_clk_enable_count(clk);
	ut_asserteq(ret, 0);

	ret = sandbox_clk_enable_count(pclk);
	ut_asserteq(ret, 0);

	/* Test clock re-parenting. */
	clk_get_by_id(dev, SANDBOX_CLK_USDHC1_SEL, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("usdhc1_sel", clk_core->name);

	pclk = clk_ccf_get_parent(clk);
	ut_assertok_ptr(pclk);
	clk_core = clk_get_core(pclk);
	if (!strcmp(clk_core->name, "pll3_60m")) {
		clkname = "pll3_80m";
		clkid = SANDBOX_CLK_PLL3_80M;
	} else {
		clkname = "pll3_60m";
		clkid = SANDBOX_CLK_PLL3_60M;
	}

	clk_get_by_id(dev, clkid, &pclk);
	ret = clk_set_parent(clk, pclk);
	ut_assertok(ret);
	pclk = clk_ccf_get_parent(clk);
	ut_assertok_ptr(pclk);
	clk_core = clk_get_core(pclk);
	ut_asserteq_str(clkname, clk_core->name);

	/* Test disabling critical clock. */
	clk_get_by_id(dev, SANDBOX_CLK_I2C_ROOT, &clk);
	clk_core = clk_get_core(clk);
	ut_asserteq_str("i2c_root", clk_core->name);

	/* Disable it, if any. */
	ret = sandbox_clk_enable_count(clk);
	for (i = 0; i < ret; i++) {
		ret = clk_disable(clk);
		ut_assertok(ret);
	}

	ret = sandbox_clk_enable_count(clk);
	ut_asserteq(ret, 0);

	clk_core->flags = CLK_IS_CRITICAL;
	ret = clk_enable(clk);
	ut_assertok(ret);

	ret = clk_disable(clk);
	ut_assertok(ret);
	ret = sandbox_clk_enable_count(clk);
	ut_asserteq(ret, 1);
	clk_core->flags &= ~CLK_IS_CRITICAL;

	ret = clk_disable(clk);
	ut_assertok(ret);
	ret = sandbox_clk_enable_count(clk);
	ut_asserteq(ret, 0);
#endif

	return 1;
}

DM_TEST(dm_test_clk_ccf, UT_TESTF_SCAN_FDT);
