// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2013 Xilinx, Inc.
 */
#include <common.h>
#include <command.h>
#include <clk.h>
#if defined(CONFIG_DM) && defined(CONFIG_CLK)
#include <dm.h>
#include <dm/device.h>
#include <dm/root.h>
#include <dm/device-internal.h>
#include <linux/clk-provider.h>
#endif

#if defined(CONFIG_DM) && defined(CONFIG_CLK)
/**
  * Since getting rid of device-per-clock, we don't track children in the parents.
  * Therefore priting the clocks as a tree would require temporarily constructing
  * the tree structure. If anyone wants to do this, they are welcome - for now,
  * just print the clocks as a flat list with the parent in brackets
  */
static void show_clks(struct clk_core *clk, int depth, int last_flag)
{
	int i, is_last;
	struct udevice *child;
	struct clk *parent;
	u32 rate;

	rate = clk->ops->get_rate(clk);

	printf(" %-12u  %8d        ", rate, clk->enable_count);

	printf("%s (%s)\n", clk->name, clk->parent ? clk->parent->name : "no parent");

}

int __weak soc_clk_dump(void)
{
	struct udevice *dev;

	printf(" Rate               Usecnt      Name (Parent)\n");
	printf("------------------------------------------\n");


	uclass_foreach_dev_probe(UCLASS_CLK, dev) {
		struct clk_array *clks = dev_get_uclass_priv(dev);
		if(clks) {
			for(int i = 0; i < clks->count; i++)
				show_clks(clks->arr[i], -1, 0);
		}
	}

	return 0;
}
#else
int __weak soc_clk_dump(void)
{
	puts("Not implemented\n");
	return 1;
}
#endif

static int do_clk_dump(struct cmd_tbl *cmdtp, int flag, int argc,
		       char *const argv[])
{
	int ret;

	ret = soc_clk_dump();
	if (ret < 0) {
		printf("Clock dump error %d\n", ret);
		ret = CMD_RET_FAILURE;
	}

	return ret;
}

#if CONFIG_IS_ENABLED(DM) && CONFIG_IS_ENABLED(CLK)
static int do_clk_setfreq(struct cmd_tbl *cmdtp, int flag, int argc,
			  char *const argv[])
{
	printf("option deprecated - clock name is no longer globally unique\n");
	return 0;
}
#endif

static struct cmd_tbl cmd_clk_sub[] = {
	U_BOOT_CMD_MKENT(dump, 1, 1, do_clk_dump, "", ""),
#if CONFIG_IS_ENABLED(DM) && CONFIG_IS_ENABLED(CLK)
	U_BOOT_CMD_MKENT(setfreq, 3, 1, do_clk_setfreq, "", ""),
#endif
};

static int do_clk(struct cmd_tbl *cmdtp, int flag, int argc,
		  char *const argv[])
{
	struct cmd_tbl *c;

	if (argc < 2)
		return CMD_RET_USAGE;

	/* Strip off leading 'clk' command argument */
	argc--;
	argv++;

	c = find_cmd_tbl(argv[0], &cmd_clk_sub[0], ARRAY_SIZE(cmd_clk_sub));

	if (c)
		return c->cmd(cmdtp, flag, argc, argv);
	else
		return CMD_RET_USAGE;
}

#ifdef CONFIG_SYS_LONGHELP
static char clk_help_text[] =
	"dump - Print clock frequencies\n"
	"clk setfreq [clk] [freq] - Set clock frequency";
#endif

U_BOOT_CMD(clk, 4, 1, do_clk, "CLK sub-system", clk_help_text);
