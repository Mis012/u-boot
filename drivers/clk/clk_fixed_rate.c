// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2016 Masahiro Yamada <yamada.masahiro@socionext.com>
 */

#define LOG_CATEGORY UCLASS_CLK

#include <common.h>
#include <clk-uclass.h>
#include <dm.h>
#include <log.h>
#include <dm/device-internal.h>
#include <linux/clk-provider.h>

void clk_fixed_rate_ofdata_to_plat_(struct udevice *dev,
				    struct clk_fixed_rate_plat *plat)
{
	struct clk_array *clks = &plat->clks;
	struct clk_fixed_rate *clk_fixed_rate = &plat->clk_fixed_rate;
	struct clk_core *clk = &clk_fixed_rate->clk;

	clk->name = dev->name;
	clk->ops = &clk_fixed_rate_ops;

	if (CONFIG_IS_ENABLED(OF_REAL))
		clk_fixed_rate->fixed_rate = dev_read_u32_default(dev, "clock-frequency",
								  0);

	clks->arr = &plat->clk_arr;
	clks->arr[0] = clk;
	clks->count = 1;

	clk_register(dev, clks);
}

/* avoid clk_enable() return -ENOSYS */
static int dummy_endis(struct clk_core *clk)
{
	return 0;
}

static ulong clk_fixed_rate_get_rate(struct clk_core *clk)
{
	return container_of(clk, struct clk_fixed_rate, clk)->fixed_rate;
}

const struct clk_core_ops clk_fixed_rate_ops = {
	.get_rate = clk_fixed_rate_get_rate,
	.enable = dummy_endis,
	.disable = dummy_endis,
};

static int clk_fixed_rate_of_to_plat(struct udevice *dev)
{
	clk_fixed_rate_ofdata_to_plat_(dev, dev_get_plat(dev));

	return 0;
}

static const struct udevice_id clk_fixed_rate_match[] = {
	{
		.compatible = "fixed-clock",
	},
	{ /* sentinel */ }
};

U_BOOT_DRIVER(fixed_clock) = {
	.name = "fixed_clock",
	.id = UCLASS_CLK,
	.of_match = clk_fixed_rate_match,
	.of_to_plat = clk_fixed_rate_of_to_plat,
	.plat_auto	= sizeof(struct clk_fixed_rate_plat),
	.ops = &ccf_clk_ops,
	.flags = DM_FLAG_PRE_RELOC,
};
