// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2019 DENX Software Engineering
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 *
 * Copyright (C) 2011 Sascha Hauer, Pengutronix <s.hauer@pengutronix.de>
 */

#define LOG_CATEGORY UCLASS_CLK

#include <common.h>
#include <clk.h>
#include <clk-uclass.h>
#include <div64.h>
#include <log.h>
#include <malloc.h>
#include <dm/device.h>
#include <dm/devres.h>
#include <linux/clk-provider.h>
#include <linux/err.h>

#include "clk.h"

static ulong clk_factor_recalc_rate(struct clk_core *clk)
{
	struct clk_fixed_factor *fix = to_clk_fixed_factor(clk);
	unsigned long parent_rate = clk_get_parent_rate(clk);
	unsigned long long int rate;

	rate = (unsigned long long int)parent_rate * fix->mult;
	do_div(rate, fix->div);
	return (ulong)rate;
}

const struct clk_core_ops ccf_clk_fixed_factor_ops = {
	.get_rate = clk_factor_recalc_rate,
};

