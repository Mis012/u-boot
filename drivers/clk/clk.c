// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2019 DENX Software Engineering
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 */

#define LOG_CATEGORY UCLASS_CLK

#include <common.h>
#include <clk.h>
#include <clk-uclass.h>
#include <log.h>
#include <dm/device.h>
#include <dm/uclass.h>
#include <dm/lists.h>
#include <dm/device-internal.h>
#include <linux/clk-provider.h>

int clk_register(struct udevice *dev, struct clk_array *clks)
{
	int ret;

	for(int i = 0; i < clks->count; i++) {
		struct clk_core *clk = clks->arr[i];

		assert(clk);
		clk->dev = dev;
		clk->enable_count = 0;

		if(clk->ops->init) {
			ret = clk->ops->init(clk);
			if(ret)
				return ret;
		}
	}

	/* Store back pointer to clk from udevice */
	/* FIXME: This is not allowed...should be allocated by driver model */
	dev_set_uclass_priv(dev, clks);

	return 0;
}

// TODO: isn't this unnecessary with CLK_SET_RATE_PARENT?
ulong clk_generic_get_rate(struct clk_core *clk)
{
	struct clk_core *parent = clk->parent;
	return parent->ops->get_rate(parent);
}

const char *clk_hw_get_name(const struct clk *hw)
{
	assert(hw);
	assert(hw->dev);

	return clk_get_core(hw)->name;
}

u32 clk_hw_get_flags(const struct clk *hw) {
	assert(hw);

	return clk_get_core(hw)->flags;
}

bool __clk_is_enabled(const struct clk *hw)
{
	assert(hw);

	return clk_get_core(hw)->enable_count;
}

bool clk_dev_binded(struct clk *clk)
{
	if (clk->dev && (dev_get_flags(clk->dev) & DM_FLAG_BOUND))
		return true;

	return false;
}

/* Helper functions for clock ops */

// TODO - check for errors on clk_get_core

ulong ccf_clk_get_rate(struct clk *clk)
{
	struct clk_core *c = clk_get_core(clk);

	if(c->ops->get_rate)
		return c->ops->get_rate(c);
	else
		return -ENOSYS;
}

ulong ccf_clk_set_rate(struct clk *clk, unsigned long rate)
{
	struct clk_core *c = clk_get_core(clk);

	if(c->ops->set_rate)
		return c->ops->set_rate(c, rate);
	else
		return -ENOSYS;
}

int ccf_clk_set_parent(struct clk *clk, struct clk *parent)
{
	int ret;

	struct clk_core *c = clk_get_core(clk);
	struct clk_core *p = clk_get_core(parent);

	if(c->ops->set_parent)
		ret = c->ops->set_parent(c, p);
	else
		ret = -ENOSYS;

	c->parent = p;

	return ret;
}

int _ccf_clk_enable(struct clk_core *c)
{
	int ret;

	if(c->parent)
		_ccf_clk_enable(c->parent);

	if(c->enable_count == 0) {
		if(c->ops->enable)
			ret = c->ops->enable(c);
		else
			ret = -ENOSYS;
	}

	c->enable_count++;

	return ret;
}

int ccf_clk_enable(struct clk *clk)
{
	struct clk_core *c = clk_get_core(clk);

	return _ccf_clk_enable(c);
}

int _ccf_clk_disable(struct clk_core *c)
{
	int ret;

	if (c->flags & CLK_IS_CRITICAL)
		return 0;

	c->enable_count--;

	if(c->enable_count < 0) {
		printf("clock %s (device %s) already disabled\n", c->name, c->dev->name);
		return 0;
	} else if(c->enable_count == 0) {
		if(c->ops->disable)
			ret = c->ops->disable(c);
		else
			ret = -ENOSYS;
	}

	if(c->parent)
		_ccf_clk_disable(c->parent);

	return ret;
}

int ccf_clk_disable(struct clk *clk)
{
	struct clk_core *c = clk_get_core(clk);

	return _ccf_clk_disable(c);
}

const struct clk_ops ccf_clk_ops = {
	.set_rate = ccf_clk_set_rate,
	.get_rate = ccf_clk_get_rate,
	.set_parent = ccf_clk_set_parent,
	.enable = ccf_clk_enable,
	.disable = ccf_clk_disable,
};
