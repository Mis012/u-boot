// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2019
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 *
 * Common Clock Framework [CCF] driver for Sandbox
 */

#include <common.h>
#include <dm.h>
#include <clk.h>
#include <malloc.h>
#include <asm/clk.h>
#include <clk-uclass.h>
#include <dm/devres.h>
#include <linux/bitops.h>
#include <linux/clk-provider.h>
#include <semi-static-struct.h>
#include <sandbox-clk.h>
#include <linux/err.h>

/*
 * Sandbox implementation of CCF primitives necessary for clk-uclass testing
 *
 * --- Sandbox PLLv3 ---
 */
struct clk_pllv3 {
	struct clk_core	clk;
	u32		div_mask;
	u32		div_shift;
};

int sandbox_clk_enable_count(struct clk *clk)
{
	struct clk_core *c = clk_get_core(clk);

	return c->enable_count;
}

static ulong clk_pllv3_get_rate(struct clk_core *clk)
{
	struct clk_core *parent = clk->parent;
	unsigned long parent_rate = parent->ops->get_rate(parent);

	return parent_rate * 24;
}

static const struct clk_core_ops sandbox_clk_pllv3_generic_ops = {
	.get_rate       = clk_pllv3_get_rate,
};

/* --- Sandbox PLLv3 --- */
/* --- Sandbox Gate  --- */
struct clk_gate2 {
	struct clk_core clk;
	bool	state;
};

#define to_clk_gate2(_clk) container_of(_clk, struct clk_gate2, clk)

static int clk_gate2_enable(struct clk_core *clk)
{
	struct clk_gate2 *gate = to_clk_gate2(clk);

	gate->state = 1;
	return 0;
}

static int clk_gate2_disable(struct clk_core *clk)
{
	struct clk_gate2 *gate = to_clk_gate2(clk);

	gate->state = 0;
	return 0;
}

static const struct clk_core_ops sandbox_clk_gate2_ops = {
	.enable = clk_gate2_enable,
	.disable = clk_gate2_disable,
	.get_rate = clk_generic_get_rate,
};

/* we are forced to use struct clk_core for the sub-clocks, so we at least utilize their .dev and .id to find the clk_composite */
#define get_composite(clk_core) to_clk_composite(clk_get_core(&(struct clk){.dev = clk_core->dev, .id = clk_core->id}))

static unsigned long sandbox_clk_composite_divider_recalc_rate(struct clk_core *clk)
{
	struct clk_divider *divider = (struct clk_divider *)to_clk_divider(clk);
	struct clk_composite *composite = get_composite(clk);
	ulong parent_rate = clk_get_parent_rate(&composite->clk);
	unsigned int val;

	val = divider->io_divider_val;
	val >>= divider->shift;
	val &= clk_div_mask(divider->width);

	return divider_recalc_rate(clk, parent_rate, val, divider->table,
				   divider->flags, divider->width);
}

static const struct clk_core_ops sandbox_clk_composite_divider_ops = {
	.get_rate = sandbox_clk_composite_divider_recalc_rate,
};

// TODO are the flags used correctly here? imx was also sus
#define sandbox_clk_composite(_x, _id, _name, _parents, _num_parents, _reg, _flags) \
	MAKE_SEMI_STATIC_STRUCT_7(struct clk_composite, _x, \
		mux, ({MAKE_SEMI_STATIC_STRUCT_9(struct clk_mux, mux, \
			reg, (void *)_reg, \
			shift, 24, \
			mask, 0x7, \
			parents, _parents, \
			num_parents, _num_parents, \
			clk.name, _name, \
			clk.id, _id, \
			clk.flags, _flags, \
			clk.ops, &clk_mux_ops \
		) &mux.clk;}), \
		\
		rate, ({MAKE_SEMI_STATIC_STRUCT_8(struct clk_divider, rate, \
			reg, (void *)_reg, \
			shift, 16, \
			width, 3, \
			flags, CLK_DIVIDER_ROUND_CLOSEST,\
			clk.name, _name, \
			clk.id, _id, \
			clk.flags, _flags, \
			clk.ops, &sandbox_clk_composite_divider_ops \
		) &rate.clk;}), \
		\
		gate, ({MAKE_SEMI_STATIC_STRUCT_6(struct clk_gate, gate, \
			reg, (void *)_reg, \
			bit_idx, 28, \
			clk.name, _name, \
			clk.id, _id, \
			clk.flags, _flags, \
			clk.ops, &clk_gate_ops \
		) &gate.clk; }), \
		\
		clk.name, _name, \
		clk.id, _id, \
		clk.flags, _flags, \
		clk.ops, &clk_composite_ops \
	)

/* --- Sandbox Gate --- */
/* The CCF core driver itself */
static const struct udevice_id sandbox_clk_ccf_test_ids[] = {
	{ .compatible = "sandbox,clk-ccf" },
	{ }
};

static int sandbox_clk_ccf_probe(struct udevice *dev)
{
	struct clk *osc_clk = malloc(sizeof(struct clk));
	clk_get_by_index(dev, 0, osc_clk);

	void *base = NULL;
	sandbox_clk_pllv3(pll3_usb_otg_clk,
			  SANDBOX_CLK_PLL3, SANDBOX_PLLV3_USB, "pll3_usb_otg", clk_get_core(osc_clk), base + 0x10, 0x3);
	sandbox_clk_fixed_factor(pl3_60m_clk,
				 SANDBOX_CLK_PLL3_60M, "pll3_60m", &pll3_usb_otg_clk.clk, 1, 8);
	sandbox_clk_fixed_factor(pll3_80m_clk,
				 SANDBOX_CLK_PLL3_80M, "pll3_80m", &pll3_usb_otg_clk.clk, 1, 6);
	/* The HW adds +1 to the divider value (2+1) is the divider */
	sandbox_clk_divider(ecspi_root_clk,
			    SANDBOX_CLK_ECSPI_ROOT, "ecspi_root", &pl3_60m_clk.clk, (2 << 19), 19, 6);
	sandbox_clk_gate(ecspi0_clk,
			 SANDBOX_CLK_ECSPI0, "ecspi0", &ecspi_root_clk.clk, 0, 0, 0);
	sandbox_clk_gate2(ecspi1_clk,
			  SANDBOX_CLK_ECSPI1, "ecspi1", &ecspi_root_clk.clk, base + 0x6c, 0);

	static const struct clk_core *const usdhc_sels[] = { &pl3_60m_clk.clk, &pll3_80m_clk.clk, };
	static const struct clk_core *const i2c_sels[] = { &pl3_60m_clk.clk, &pll3_80m_clk.clk, };

	/* Select 'pll3_60m' */
	sandbox_clk_mux(usdhc1_sel_clk,
			SANDBOX_CLK_USDHC1_SEL, "usdhc1_sel", 0, 16, 1, usdhc_sels, ARRAY_SIZE(usdhc_sels));
	/* Select 'pll3_80m' */
	sandbox_clk_mux(usdhc2_sel_clk,
			SANDBOX_CLK_USDHC2_SEL, "usdhc2_sel", BIT(17), 17, 1, usdhc_sels, ARRAY_SIZE(usdhc_sels));
	sandbox_clk_composite(i2c_clk,
			      SANDBOX_CLK_I2C, "i2c", i2c_sels, ARRAY_SIZE(i2c_sels), (BIT(28) | BIT(24) | BIT(16)), CLK_SET_RATE_UNGATE);
	sandbox_clk_gate2(i2c_root_clk,
			  SANDBOX_CLK_I2C_ROOT, "i2c_root", &i2c_clk.clk, base + 0x7c, 0);

	static struct clk_core *sandbox_clks[] = {
		&pll3_usb_otg_clk.clk,
		&pl3_60m_clk.clk,
		&pll3_80m_clk.clk,
		&ecspi_root_clk.clk,
		&ecspi0_clk.clk,
		&ecspi1_clk.clk,
		&usdhc1_sel_clk.clk,
		&usdhc2_sel_clk.clk,
		&i2c_clk.clk,
		&i2c_root_clk.clk,
	};

	static struct clk_array clks = {
		.arr = sandbox_clks,
		.count = ARRAY_SIZE(sandbox_clks),
	};

	return clk_register(dev, &clks);

	return 0;
}

U_BOOT_DRIVER(sandbox_clk_ccf) = {
	.name = "sandbox_clk_ccf",
	.id = UCLASS_CLK,
	.probe = sandbox_clk_ccf_probe,
	.of_match = sandbox_clk_ccf_test_ids,
	.ops = &ccf_clk_ops,
};
