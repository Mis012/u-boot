// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2010-2011 Canonical Ltd <jeremy.kerr@canonical.com>
 * Copyright (C) 2011-2012 Mike Turquette, Linaro Ltd <mturquette@linaro.org>
 * Copyright 2019 NXP
 *
 * Gated clock implementation
 */

#define LOG_CATEGORY UCLASS_CLK

#include <common.h>
#include <clk.h>
#include <log.h>
#include <clk-uclass.h>
#include <malloc.h>
#include <asm/io.h>
#include <dm/device.h>
#include <dm/device_compat.h>
#include <dm/devres.h>
#include <linux/bitops.h>
#include <linux/clk-provider.h>
#include <linux/err.h>

#include "clk.h"

/**
 * DOC: basic gatable clock which can gate and ungate it's output
 *
 * Traits of this clock:
 * prepare - clk_(un)prepare only ensures parent is (un)prepared
 * enable - clk_enable and clk_disable are functional & control gating
 * rate - inherits rate from parent.  No clk_set_rate support
 * parent - fixed parent.  No clk_set_parent support
 */

/*
 * It works on following logic:
 *
 * For enabling clock, enable = 1
 *	set2dis = 1	-> clear bit	-> set = 0
 *	set2dis = 0	-> set bit	-> set = 1
 *
 * For disabling clock, enable = 0
 *	set2dis = 1	-> set bit	-> set = 1
 *	set2dis = 0	-> clear bit	-> set = 0
 *
 * So, result is always: enable xor set2dis.
 */
static void clk_gate_endisable(struct clk_core *clk, int enable)
{
	struct clk_gate *gate = to_clk_gate(clk);
	int set = gate->flags & CLK_GATE_SET_TO_DISABLE ? 1 : 0;
	u32 reg;

	set ^= enable;

	if (gate->flags & CLK_GATE_HIWORD_MASK) {
		reg = BIT(gate->bit_idx + 16);
		if (set)
			reg |= BIT(gate->bit_idx);
	} else {
#if IS_ENABLED(CONFIG_SANDBOX_CLK_CCF)
		reg = gate->io_gate_val;
#else
		reg = readl(gate->reg);
#endif

		if (set)
			reg |= BIT(gate->bit_idx);
		else
			reg &= ~BIT(gate->bit_idx);
	}

	writel(reg, gate->reg);
}

static int clk_gate_enable(struct clk_core *clk)
{
	clk_gate_endisable(clk, 1);

	return 0;
}

static int clk_gate_disable(struct clk_core *clk)
{
	clk_gate_endisable(clk, 0);

	return 0;
}

int clk_gate_is_enabled(struct clk_core *clk)
{
	struct clk_gate *gate = to_clk_gate(clk);
	u32 reg;

#if IS_ENABLED(CONFIG_SANDBOX_CLK_CCF)
	reg = gate->io_gate_val;
#else
	reg = readl(gate->reg);
#endif

	/* if a set bit disables this clk, flip it before masking */
	if (gate->flags & CLK_GATE_SET_TO_DISABLE)
		reg ^= BIT(gate->bit_idx);

	reg &= BIT(gate->bit_idx);

	return reg ? 1 : 0;
}

static int clk_gate_init(struct clk_core *clk) {
	struct clk_gate *gate = to_clk_gate(clk);

	if (gate->flags & CLK_GATE_HIWORD_MASK) {
		if (gate->bit_idx > 15) {
			dev_err(clk->dev, "gate bit exceeds LOWORD field\n");
			return -EINVAL;
		}
	}

#if IS_ENABLED(CONFIG_SANDBOX_CLK_CCF)
	gate->io_gate_val = (u32)gate->reg;
#endif

	return 0;
}

const struct clk_core_ops clk_gate_ops = {
	.init = clk_gate_init,
	.enable = clk_gate_enable,
	.disable = clk_gate_disable,
	.get_rate = clk_generic_get_rate,
};
