// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2013 NVIDIA CORPORATION.  All rights reserved.
 * Copyright 2019 NXP
 */

#define LOG_CATEGORY UCLASS_CLK

#include <common.h>
#include <clk.h>
#include <clk-uclass.h>
#include <log.h>
#include <malloc.h>
#include <asm/io.h>
#include <dm/device.h>
#include <dm/devres.h>
#include <linux/clk-provider.h>
#include <linux/err.h>

#include "clk.h"

static u8 clk_composite_get_parent(struct clk_core *clk)
{
	struct clk_composite *composite = to_clk_composite(clk);
	struct clk_core *mux = composite->mux;

	if (mux)
		return clk_mux_get_parent(mux);
	else
		return 0;
}

static int clk_composite_set_parent(struct clk_core *clk, struct clk_core *parent)
{
	struct clk_composite *composite = to_clk_composite(clk);
	struct clk_core *mux = composite->mux;

	if (!mux)
		return -ENOSYS;

	return mux->ops->set_parent(mux, parent);
}

static unsigned long clk_composite_recalc_rate(struct clk_core *clk)
{
	struct clk_composite *composite = to_clk_composite(clk);
	struct clk_core *clk_rate = composite->rate;

	if (clk_rate && clk_rate->ops)
		return clk_rate->ops->get_rate(clk_rate);
	else
		return clk_get_parent_rate(clk);
}

static ulong clk_composite_set_rate(struct clk_core *clk, unsigned long rate)
{
	struct clk_composite *composite = to_clk_composite(clk);
	struct clk_core *clk_rate = composite->rate;

	if (clk_rate && clk_rate->ops)
		return clk_rate->ops->set_rate(clk_rate, rate);
	else
		return clk->ops->get_rate(clk);
}

static int clk_composite_enable(struct clk_core *clk)
{
	struct clk_composite *composite = to_clk_composite(clk);
	struct clk_core *gate = composite->gate;

	if (gate && gate->ops)
		return gate->ops->enable(gate);
	else
		return 0;
}

static int clk_composite_disable(struct clk_core *clk)
{
	struct clk_composite *composite = to_clk_composite(clk);
	struct clk_core *gate = composite->gate;

	if (gate && gate->ops)
		return gate->ops->disable(gate);
	else
		return 0;
}

static int clk_composite_init(struct clk_core *clk) {
	struct clk_composite *composite = to_clk_composite(clk);
	if(composite->mux)
		composite->mux->dev = clk->dev;
	if(composite->rate)
		composite->rate->dev = clk->dev;
	if(composite->gate)
		composite->gate->dev = clk->dev;

	if(!clk->parent && composite->mux)
		clk->parent = to_clk_mux(composite->mux)->parents[clk_composite_get_parent(clk)];

	return 0;
}

const struct clk_core_ops clk_composite_ops = {
	.init = clk_composite_init,
	.set_parent = clk_composite_set_parent,
	.get_rate = clk_composite_recalc_rate,
	.set_rate = clk_composite_set_rate,
	.enable = clk_composite_enable,
	.disable = clk_composite_disable,
};
