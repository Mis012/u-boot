// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2019 DENX Software Engineering
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 */
#ifndef __MACH_IMX_CLK_H
#define __MACH_IMX_CLK_H

#include <linux/clk-provider.h>

#define imx_8m_clk_composite_add_base(_clk, _base) *(uintptr_t *)&to_clk_mux(_clk.mux)->reg += (uintptr_t)_base; *(uintptr_t *)&to_clk_divider(_clk.rate)->reg += (uintptr_t)_base; *(uintptr_t *)&to_clk_gate(_clk.gate)->reg += (uintptr_t)_base;

/* TODO: is there a cleaner way to do this */
/* pllv3 ops, passed to the imx_clk_pllv3 macro as opaque types */
#define IMX_PLLV3_GENERIC	&clk_pllv3_generic_ops
#define IMX_PLLV3_GENERICV2	&clk_pllv3_genericv2_ops
#define IMX_PLLV3_SYS		&clk_pllv3_sys_ops
#define IMX_PLLV3_USB		&clk_pllv3_generic_ops
#define IMX_PLLV3_AV		&clk_pllv3_av_ops
#define IMX_PLLV3_ENET		&clk_pllv3_enet_ops

/* TODO comment - stuff for clk-composite-8m */
#define PCG_PREDIV_SHIFT	16
#define PCG_PREDIV_WIDTH	3

#define PCG_PCS_SHIFT		24
#define PCG_PCS_MASK		0x7

#define PCG_CGC_SHIFT		28

/* NOTE: Rate table should be kept sorted in descending order. */
struct imx_pll14xx_rate_table {
	unsigned int rate;
	unsigned int pdiv;
	unsigned int mdiv;
	unsigned int sdiv;
	unsigned int kdiv;
};

struct imx_pll14xx_clk {
	const struct clk_core_ops ops;
	const struct imx_pll14xx_rate_table *rate_table;
	int rate_count;
};

struct clk_pll14xx {
	struct clk_core			clk;
	void __iomem			*base;
	const struct imx_pll14xx_clk*	pll_clk;
};

struct clk_pfd {
	struct clk_core	clk;
	void __iomem	*reg;
	u8		idx;
};

struct clk_gate2 {
	struct clk_core	clk;
	void __iomem	*reg;
	u8		bit_idx;
	u8		cgr_val;
	u8		flags;
	unsigned int	*share_count;
};

struct clk_pllv3 {
	struct clk_core	clk;
	void __iomem	*base;
	u32		power_bit;
	bool		powerup_set;
	u32		lock_bit;
	u32		enable_bit;
	u32		div_mask;
	u32		div_shift;
	unsigned long   ref_clock;
};

extern const struct clk_core_ops clk_pfd_ops;
extern const struct clk_core_ops clk_gate2_ops;
extern const struct clk_core_ops clk_pllv3_generic_ops;
extern const struct clk_core_ops clk_pllv3_genericv2_ops;
extern const struct clk_core_ops clk_pllv3_sys_ops;
extern const struct clk_core_ops clk_pllv3_av_ops;
extern const struct clk_core_ops clk_pllv3_enet_ops;
extern const struct clk_core_ops clk_pll1416x_ops;
extern const struct clk_core_ops clk_pll1443x_ops;
extern const struct clk_core_ops imx8m_clk_composite_divider_ops;

extern const struct imx_pll14xx_clk imx_1443x_pll;
extern const struct imx_pll14xx_clk imx_1443x_dram_pll;
extern const struct imx_pll14xx_clk imx_1416x_pll;

#define imx_clk_pll14xx(_id, _name, _parent, _base, _pll_clk, _ops) \
	{ \
		.base = (void *)_base, \
		.pll_clk = _pll_clk, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.ops = _ops \
		} \
	}

#define imx_clk_pllv3(_id, _ops, _name, _parent, _base, _div_mask) \
	{ \
		.power_bit = (_ops == IMX_PLLV3_GENERICV2) ? BM_PLL_POWER_V2 : PM_BLL_POWER, \
		.enable_bit = BM_PLL_ENABLE, \
		.lock_bit = (_ops == IMX_PLLV3_GENERICV2) ? BM_PLL_LOCK_V2 : BM_PLL_LOCK, \
		.div_shift = (_ops == IMX_PLLV3_USB) ? 1 : 0, \
		.powerup_set = (_ops == IMX_PLLV3_USB) ? true : false, \
		.ref_clock = (_ops == IMX_PLLV3_ENET) ? 500000000 : 0, \
		.base = (void *)_base, \
		.div_mask = _div_mask, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.ops = _ops \
		} \
	}


/* NOTE: clk_register_gate2 wasn't previously setting clk.flags, we assume this was an error */

#define imx_clk_gate2(_id, _name, _parent, _reg, _shift) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.cgr_val = 0x3, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT, \
			.ops = &clk_gate2_ops \
		} \
	}

#define imx_clk_gate2_shared(_id, _name, _parent, _reg, _shift, _share_count) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.cgr_val = 0x3, \
		.share_count = _share_count, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT, \
			.ops = &clk_gate2_ops \
		} \
	}

#define imx_clk_gate2_shared2(_id, _name, _parent, _reg, _shift, _share_count) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.cgr_val = 0x3, \
		.share_count = _share_count, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_gate2_ops \
		} \
	}

#define imx_clk_gate4(_id, _name, _parent, _reg, _shift) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.cgr_val = 0x3, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_gate2_ops \
		} \
	}

#define imx_clk_gate4_flags(_id, _name, _parent, _reg, _shift, _flags) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.cgr_val = 0x3, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = _flags | CLK_SET_RATE_PARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_gate2_ops \
		} \
	}

#define imx_clk_fixed_rate(_id, _name, _rate) \
	{ \
		.fixed_rate = _rate, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.ops = &clk_fixed_rate_raw_ops \
		} \
	}

#define imx_clk_fixed_factor(_id, _name, _parent, _mult, _div) \
	{ \
		.mult = _mult, \
		.div = _div, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT, \
			.ops = &ccf_clk_fixed_factor_ops \
		} \
	}

#define imx_clk_divider(_id, _name, _parent, _reg, _shift, _width) \
	{ \
		.reg = (void *)_reg, \
		.shift = _shift, \
		.width = _width, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT, \
			.ops = &clk_divider_ops \
		} \
	}

#define imx_clk_busy_divider(_id, _name, _parent, _reg, _shift, _width, _busy_reg, _busy_shift) \
	imx_clk_divider(_id, _name, _parent, _reg, _shift, _width)

#define imx_clk_divider2(_id, _name, _parent, _reg, _shift, _width) \
	{ \
		.reg = (void *)_reg, \
		.shift = _shift, \
		.width = _width, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_divider_ops \
		} \
	}

#define imx_clk_pfd(_id, _name, _parent, _reg, _idx) \
	{ \
		.reg = (void *)_reg, \
		.idx = _idx, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.ops = &clk_pfd_ops \
		} \
	}

#define imx_clk_mux_flags(_id, _name, _reg, _shift, _width, _parents, _num_parents, _flags) \
	{ \
		.reg = (void *)_reg, \
		.shift = _shift, \
		.mask = BIT(_width) - 1, \
		.parent_names = _parents, \
		.num_parents = _num_parents, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.flags = _flags | CLK_SET_RATE_NO_REPARENT, \
			.ops = &clk_mux_ops \
		} \
	}

#define imx_clk_mux2_flags(_id, _name, _reg, _shift, _width, _parents, _num_parents, _flags) \
	{ \
		.reg = (void *)_reg, \
		.shift = _shift, \
		.mask = BIT(_width) - 1, \
		.parent_names = _parents, \
		.num_parents = _num_parents, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.flags = _flags | CLK_SET_RATE_NO_REPARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_mux_ops \
		} \
	}

#define imx_clk_mux(_id, _name, _reg, _shift, _width, _parents, _num_parents) \
	{ \
		.reg = (void *)_reg, \
		.shift = _shift, \
		.mask = BIT(_width) - 1, \
		.parent_names = _parents, \
		.num_parents = _num_parents, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.flags = CLK_SET_RATE_NO_REPARENT, \
			.ops = &clk_mux_ops \
		} \
	}

#define imx_clk_busy_mux(_id, _name, _reg, _shift, _width, _busy_reg, _busy_shift, _parents, _num_parents) \
	imx_clk_mux(_id, _name, _reg, _shift, _width, _parents, _num_parents)

#define imx_clk_mux2(_id, _name, _reg, _shift, _width, _parents, _num_parents) \
	{ \
		.reg = (void *)_reg, \
		.shift = _shift, \
		.mask = BIT(_width) - 1, \
		.parent_names = _parents, \
		.num_parents = _num_parents, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.flags = CLK_SET_RATE_NO_REPARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_mux_ops \
		} \
	}

#define imx_clk_gate(_id, _name, _parent, _reg, _shift) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT, \
			.ops = &clk_gate_ops \
		} \
	}

#define imx_clk_gate_flags(_id, _name, _parent, _reg, _shift, _flags) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = _flags | CLK_SET_RATE_PARENT, \
			.ops = &clk_gate_ops \
		} \
	}

#define imx_clk_gate3(_id, _name, _parent, _reg, _shift) \
	{ \
		.reg = (void *)_reg, \
		.bit_idx = _shift, \
		.clk = { \
			.name = _name, \
			.id = _id, \
			.parent = _parent, \
			.flags = CLK_SET_RATE_PARENT | CLK_OPS_PARENT_ENABLE, \
			.ops = &clk_gate_ops \
		} \
	}

/* imx8m_clk_composite_flags used to put clk_core flags in the wrapper struct flags, we assume this was an error */
#define imx8m_clk_composite_flags(_id, _name, _parents, _num_parents, _reg, _flags) \
	{ \
		.mux = &((struct clk_mux) { \
			.reg = (void *)_reg, \
			.shift = PCG_PCS_SHIFT, \
			.mask = PCG_PCS_MASK, \
			.parent_names = _parents, \
			.num_parents = _num_parents, \
			.clk = { \
				.name = _name, \
				.id = _id, \
				.flags = _flags, \
				.ops = &clk_mux_ops \
			} \
		}).clk,\
		\
		.rate = &((struct clk_divider) { \
			.reg = (void *)_reg, \
			.shift = PCG_PREDIV_SHIFT, \
			.width = PCG_PREDIV_WIDTH, \
			.flags = CLK_DIVIDER_ROUND_CLOSEST,\
			.clk = { \
				.name = _name, \
				.id = _id, \
				.flags = _flags, \
				.ops = &imx8m_clk_composite_divider_ops \
			} \
		}).clk,\
		\
		.gate = &((struct clk_gate) { \
			.reg = (void *)_reg, \
			.bit_idx = PCG_CGC_SHIFT, \
			.clk = { \
				.name = _name, \
				.id = _id, \
				.flags = _flags, \
				.ops = &clk_gate_ops \
			} \
		}).clk,\
		\
		.clk = { \
			.name = _name, \
			.id = _id, \
			.flags = _flags, \
			.ops = &clk_composite_ops \
		} \
	}

#define __imx8m_clk_composite(_id, name, parent_names, reg, flags) \
	imx8m_clk_composite_flags(_id, name, parent_names, \
		ARRAY_SIZE(parent_names), reg, \
		flags | CLK_SET_RATE_NO_REPARENT | CLK_OPS_PARENT_ENABLE)

#define imx8m_clk_composite(_id, name, parent_names, reg) \
	__imx8m_clk_composite(_id, name, parent_names, reg, 0)

#define imx8m_clk_composite_critical(_id, name, parent_names, reg) \
	__imx8m_clk_composite(_id, name, parent_names, reg, CLK_IS_CRITICAL)

#endif /* __MACH_IMX_CLK_H */
