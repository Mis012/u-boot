// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2019 DENX Software Engineering
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 *
 * Copyright 2012 Freescale Semiconductor, Inc.
 * Copyright 2012 Linaro Ltd.
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <common.h>
#include <asm/io.h>
#include <malloc.h>
#include <clk-uclass.h>
#include <dm/device.h>
#include <dm/devres.h>
#include <linux/clk-provider.h>
#include <div64.h>
#include <clk.h>
#include "clk.h"
#include <linux/err.h>

#define to_clk_pfd(_clk) container_of(_clk, struct clk_pfd, clk)

#define SET	0x4
#define CLR	0x8
#define OTG	0xc

static unsigned long clk_pfd_recalc_rate(struct clk_core *clk)
{
	struct clk_pfd *pfd =
		to_clk_pfd(clk);
	struct clk_core *parent = clk->parent;
	unsigned long parent_rate = parent->ops->get_rate(parent);
	u64 tmp = parent_rate;
	u8 frac = (readl(pfd->reg) >> (pfd->idx * 8)) & 0x3f;

	tmp *= 18;
	do_div(tmp, frac);

	return tmp;
}

static unsigned long clk_pfd_set_rate(struct clk_core *clk, unsigned long rate)
{
	struct clk_pfd *pfd = to_clk_pfd(clk);
	struct clk_core *parent = clk->parent;
	unsigned long parent_rate = parent->ops->get_rate(parent);
	u64 tmp = parent_rate;
	u8 frac;

	tmp = tmp * 18 + rate / 2;
	do_div(tmp, rate);
	frac = tmp;
	if (frac < 12)
		frac = 12;
	else if (frac > 35)
		frac = 35;

	writel(0x3f << (pfd->idx * 8), pfd->reg + CLR);
	writel(frac << (pfd->idx * 8), pfd->reg + SET);

	return 0;
}

const struct clk_core_ops clk_pfd_ops = {
	.get_rate	= clk_pfd_recalc_rate,
	.set_rate	= clk_pfd_set_rate,
};
