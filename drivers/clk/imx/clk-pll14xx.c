// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright 2017-2019 NXP.
 *
 * Peng Fan <peng.fan@nxp.com>
 */

#include <common.h>
#include <asm/io.h>
#include <malloc.h>
#include <clk-uclass.h>
#include <dm/device.h>
#include <dm/devres.h>
#include <linux/bitops.h>
#include <linux/clk-provider.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/iopoll.h>
#include <clk.h>
#include <div64.h>

#include "clk.h"

#define GNRL_CTL	0x0
#define DIV_CTL		0x4
#define LOCK_STATUS	BIT(31)
#define LOCK_SEL_MASK	BIT(29)
#define CLKE_MASK	BIT(11)
#define RST_MASK	BIT(9)
#define BYPASS_MASK	BIT(4)
#define MDIV_SHIFT	12
#define MDIV_MASK	GENMASK(21, 12)
#define PDIV_SHIFT	4
#define PDIV_MASK	GENMASK(9, 4)
#define SDIV_SHIFT	0
#define SDIV_MASK	GENMASK(2, 0)
#define KDIV_SHIFT	0
#define KDIV_MASK	GENMASK(15, 0)

#define LOCK_TIMEOUT_US		10000

#define to_clk_pll14xx(_clk) container_of(_clk, struct clk_pll14xx, clk)

#define PLL_1416X_RATE(_rate, _m, _p, _s)		\
	{						\
		.rate	=	(_rate),		\
		.mdiv	=	(_m),			\
		.pdiv	=	(_p),			\
		.sdiv	=	(_s),			\
	}

#define PLL_1443X_RATE(_rate, _m, _p, _s, _k)		\
	{						\
		.rate	=	(_rate),		\
		.mdiv	=	(_m),			\
		.pdiv	=	(_p),			\
		.sdiv	=	(_s),			\
		.kdiv	=	(_k),			\
	}

static const struct imx_pll14xx_rate_table imx_pll1416x_tbl[] = {
	PLL_1416X_RATE(1800000000U, 225, 3, 0),
	PLL_1416X_RATE(1600000000U, 200, 3, 0),
	PLL_1416X_RATE(1500000000U, 375, 3, 1),
	PLL_1416X_RATE(1400000000U, 350, 3, 1),
	PLL_1416X_RATE(1200000000U, 300, 3, 1),
	PLL_1416X_RATE(1000000000U, 250, 3, 1),
	PLL_1416X_RATE(800000000U,  200, 3, 1),
	PLL_1416X_RATE(750000000U,  250, 2, 2),
	PLL_1416X_RATE(700000000U,  350, 3, 2),
	PLL_1416X_RATE(600000000U,  300, 3, 2),
};

const struct imx_pll14xx_rate_table imx_pll1443x_tbl[] = {
	PLL_1443X_RATE(1039500000U, 173, 2, 1, 16384),
	PLL_1443X_RATE(650000000U, 325, 3, 2, 0),
	PLL_1443X_RATE(594000000U, 198, 2, 2, 0),
	PLL_1443X_RATE(519750000U, 173, 2, 2, 16384),
	PLL_1443X_RATE(393216000U, 262, 2, 3, 9437),
	PLL_1443X_RATE(361267200U, 361, 3, 3, 17511),
};

const struct imx_pll14xx_clk imx_1443x_pll __initdata = {
	.ops = &clk_pll1443x_ops,
	.rate_table = imx_pll1443x_tbl,
	.rate_count = ARRAY_SIZE(imx_pll1443x_tbl),
};
EXPORT_SYMBOL_GPL(imx_1443x_pll);

const struct imx_pll14xx_clk imx_1443x_dram_pll __initdata = {
	.ops = &clk_pll1443x_ops,
	.rate_table = imx_pll1443x_tbl,
	.rate_count = ARRAY_SIZE(imx_pll1443x_tbl),
};
EXPORT_SYMBOL_GPL(imx_1443x_dram_pll);

const struct imx_pll14xx_clk imx_1416x_pll __initdata = {
	.ops = &clk_pll1416x_ops,
	.rate_table = imx_pll1416x_tbl,
	.rate_count = ARRAY_SIZE(imx_pll1416x_tbl),
};
EXPORT_SYMBOL_GPL(imx_1416x_pll);

static const struct imx_pll14xx_rate_table *imx_get_pll_settings(
		struct clk_pll14xx *pll, unsigned long rate)
{
	const struct imx_pll14xx_rate_table *rate_table = pll->pll_clk->rate_table;
	int i;

	for (i = 0; i < pll->pll_clk->rate_count; i++)
		if (rate == rate_table[i].rate)
			return &rate_table[i];

	return NULL;
}

static unsigned long clk_pll1416x_recalc_rate(struct clk_core *clk)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(clk);
	u64 fvco = clk_get_parent_rate(clk);
	u32 mdiv, pdiv, sdiv, pll_div;

	pll_div = readl(pll->base + 4);
	mdiv = (pll_div & MDIV_MASK) >> MDIV_SHIFT;
	pdiv = (pll_div & PDIV_MASK) >> PDIV_SHIFT;
	sdiv = (pll_div & SDIV_MASK) >> SDIV_SHIFT;

	fvco *= mdiv;
	do_div(fvco, pdiv << sdiv);

	return fvco;
}

static unsigned long clk_pll1443x_recalc_rate(struct clk_core *clk)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(clk);
	u64 fvco = clk_get_parent_rate(clk);
	u32 mdiv, pdiv, sdiv, pll_div_ctl0, pll_div_ctl1;
	short int kdiv;

	pll_div_ctl0 = readl(pll->base + 4);
	pll_div_ctl1 = readl(pll->base + 8);
	mdiv = (pll_div_ctl0 & MDIV_MASK) >> MDIV_SHIFT;
	pdiv = (pll_div_ctl0 & PDIV_MASK) >> PDIV_SHIFT;
	sdiv = (pll_div_ctl0 & SDIV_MASK) >> SDIV_SHIFT;
	kdiv = pll_div_ctl1 & KDIV_MASK;

	/* fvco = (m * 65536 + k) * Fin / (p * 65536) */
	fvco *= (mdiv * 65536 + kdiv);
	pdiv *= 65536;

	do_div(fvco, pdiv << sdiv);

	return fvco;
}

static inline bool clk_pll1416x_mp_change(const struct imx_pll14xx_rate_table *rate,
					  u32 pll_div)
{
	u32 old_mdiv, old_pdiv;

	old_mdiv = (pll_div & MDIV_MASK) >> MDIV_SHIFT;
	old_pdiv = (pll_div & PDIV_MASK) >> PDIV_SHIFT;

	return rate->mdiv != old_mdiv || rate->pdiv != old_pdiv;
}

static inline bool clk_pll1443x_mpk_change(const struct imx_pll14xx_rate_table *rate,
					   u32 pll_div_ctl0, u32 pll_div_ctl1)
{
	u32 old_mdiv, old_pdiv, old_kdiv;

	old_mdiv = (pll_div_ctl0 & MDIV_MASK) >> MDIV_SHIFT;
	old_pdiv = (pll_div_ctl0 & PDIV_MASK) >> PDIV_SHIFT;
	old_kdiv = (pll_div_ctl1 & KDIV_MASK) >> KDIV_SHIFT;

	return rate->mdiv != old_mdiv || rate->pdiv != old_pdiv ||
		rate->kdiv != old_kdiv;
}

static inline bool clk_pll1443x_mp_change(const struct imx_pll14xx_rate_table *rate,
					  u32 pll_div_ctl0, u32 pll_div_ctl1)
{
	u32 old_mdiv, old_pdiv, old_kdiv;

	old_mdiv = (pll_div_ctl0 & MDIV_MASK) >> MDIV_SHIFT;
	old_pdiv = (pll_div_ctl0 & PDIV_MASK) >> PDIV_SHIFT;
	old_kdiv = (pll_div_ctl1 & KDIV_MASK) >> KDIV_SHIFT;

	return rate->mdiv != old_mdiv || rate->pdiv != old_pdiv ||
		rate->kdiv != old_kdiv;
}

static int clk_pll14xx_wait_lock(struct clk_pll14xx *pll)
{
	u32 val;

	return readl_poll_timeout(pll->base, val, val & LOCK_TIMEOUT_US,
			LOCK_TIMEOUT_US);
}

static ulong clk_pll1416x_set_rate(struct clk_core *clk, unsigned long drate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(clk);
	const struct imx_pll14xx_rate_table *rate;
	u32 tmp, div_val;
	int ret;

	rate = imx_get_pll_settings(pll, drate);
	if (!rate) {
		pr_err("%s: Invalid rate : %lu for pll clk %s\n", __func__,
		       drate, "xxxx");
		return -EINVAL;
	}

	tmp = readl(pll->base + 4);

	if (!clk_pll1416x_mp_change(rate, tmp)) {
		tmp &= ~(SDIV_MASK) << SDIV_SHIFT;
		tmp |= rate->sdiv << SDIV_SHIFT;
		writel(tmp, pll->base + 4);

		return clk_pll1416x_recalc_rate(clk);
	}

	/* Bypass clock and set lock to pll output lock */
	tmp = readl(pll->base);
	tmp |= LOCK_SEL_MASK;
	writel(tmp, pll->base);

	/* Enable RST */
	tmp &= ~RST_MASK;
	writel(tmp, pll->base);

	/* Enable BYPASS */
	tmp |= BYPASS_MASK;
	writel(tmp, pll->base);


	div_val = (rate->mdiv << MDIV_SHIFT) | (rate->pdiv << PDIV_SHIFT) |
		(rate->sdiv << SDIV_SHIFT);
	writel(div_val, pll->base + 0x4);

	/*
	 * According to SPEC, t3 - t2 need to be greater than
	 * 1us and 1/FREF, respectively.
	 * FREF is FIN / Prediv, the prediv is [1, 63], so choose
	 * 3us.
	 */
	udelay(3);

	/* Disable RST */
	tmp |= RST_MASK;
	writel(tmp, pll->base);

	/* Wait Lock */
	ret = clk_pll14xx_wait_lock(pll);
	if (ret)
		return ret;

	/* Bypass */
	tmp &= ~BYPASS_MASK;
	writel(tmp, pll->base);

	return clk_pll1416x_recalc_rate(clk);
}

static ulong clk_pll1443x_set_rate(struct clk_core *clk, unsigned long drate)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(clk);
	const struct imx_pll14xx_rate_table *rate;
	u32 tmp, div_val;
	int ret;

	rate = imx_get_pll_settings(pll, drate);
	if (!rate) {
		pr_err("%s: Invalid rate : %lu for pll clk %s\n", __func__,
		       drate, "===");
		return -EINVAL;
	}

	tmp = readl(pll->base + 4);
	div_val = readl(pll->base + 8);

	if (!clk_pll1443x_mpk_change(rate, tmp, div_val)) {
		tmp &= ~(SDIV_MASK) << SDIV_SHIFT;
		tmp |= rate->sdiv << SDIV_SHIFT;
		writel(tmp, pll->base + 4);

		return clk_pll1443x_recalc_rate(clk);
	}

	tmp = readl(pll->base);

	/* Enable RST */
	tmp &= ~RST_MASK;
	writel(tmp, pll->base);

	/* Enable BYPASS */
	tmp |= BYPASS_MASK;
	writel(tmp, pll->base);

	div_val = (rate->mdiv << MDIV_SHIFT) | (rate->pdiv << PDIV_SHIFT) |
		(rate->sdiv << SDIV_SHIFT);
	writel(div_val, pll->base + 0x4);
	writel(rate->kdiv << KDIV_SHIFT, pll->base + 0x8);

	/*
	 * According to SPEC, t3 - t2 need to be greater than
	 * 1us and 1/FREF, respectively.
	 * FREF is FIN / Prediv, the prediv is [1, 63], so choose
	 * 3us.
	 */
	udelay(3);

	/* Disable RST */
	tmp |= RST_MASK;
	writel(tmp, pll->base);

	/* Wait Lock*/
	ret = clk_pll14xx_wait_lock(pll);
	if (ret)
		return ret;

	/* Bypass */
	tmp &= ~BYPASS_MASK;
	writel(tmp, pll->base);

	return clk_pll1443x_recalc_rate(clk);
}

static int clk_pll14xx_prepare(struct clk_core *clk)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(clk);
	u32 val;

	/*
	 * RESETB = 1 from 0, PLL starts its normal
	 * operation after lock time
	 */
	val = readl(pll->base + GNRL_CTL);
	val |= RST_MASK;
	writel(val, pll->base + GNRL_CTL);

	return clk_pll14xx_wait_lock(pll);
}

static int clk_pll14xx_unprepare(struct clk_core *clk)
{
	struct clk_pll14xx *pll = to_clk_pll14xx(clk);
	u32 val;

	/*
	 * Set RST to 0, power down mode is enabled and
	 * every digital block is reset
	 */
	val = readl(pll->base + GNRL_CTL);
	val &= ~RST_MASK;
	writel(val, pll->base + GNRL_CTL);

	return 0;
}

const struct clk_core_ops clk_pll1416x_ops = {
	.enable		= clk_pll14xx_prepare,
	.disable	= clk_pll14xx_unprepare,
	.set_rate	= clk_pll1416x_set_rate,
	.get_rate	= clk_pll1416x_recalc_rate,
};

const struct clk_core_ops clk_pll1443x_ops = {
	.enable		= clk_pll14xx_prepare,
	.disable	= clk_pll14xx_unprepare,
	.set_rate	= clk_pll1443x_set_rate,
	.get_rate	= clk_pll1443x_recalc_rate,
};
