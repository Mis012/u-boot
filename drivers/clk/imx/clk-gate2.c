// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2019 DENX Software Engineering
 * Lukasz Majewski, DENX Software Engineering, lukma@denx.de
 *
 * Copyright (C) 2010-2011 Canonical Ltd <jeremy.kerr@canonical.com>
 * Copyright (C) 2011-2012 Mike Turquette, Linaro Ltd <mturquette@linaro.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Gated clock implementation
 *
 */

#include <common.h>
#include <asm/io.h>
#include <malloc.h>
#include <clk-uclass.h>
#include <dm/device.h>
#include <dm/devres.h>
#include <linux/bug.h>
#include <linux/clk-provider.h>
#include <clk.h>
#include "clk.h"
#include <linux/err.h>

#define to_clk_gate2(_clk) container_of(_clk, struct clk_gate2, clk)

static int clk_gate2_enable(struct clk_core *clk)
{
	struct clk_gate2 *gate = to_clk_gate2(clk);
	u32 reg;

	if (gate->share_count && (*gate->share_count)++ > 0)
		return 0;

	reg = readl(gate->reg);
	reg &= ~(3 << gate->bit_idx);
	reg |= gate->cgr_val << gate->bit_idx;
	writel(reg, gate->reg);

	return 0;
}

static int clk_gate2_disable(struct clk_core *clk)
{
	struct clk_gate2 *gate = to_clk_gate2(clk);
	u32 reg;

	if (gate->share_count) {
		if (WARN_ON(*gate->share_count == 0))
			return 0;
		else if (--(*gate->share_count) > 0)
			return 0;
	}

	reg = readl(gate->reg);
	reg &= ~(3 << gate->bit_idx);
	writel(reg, gate->reg);

	return 0;
}

static ulong clk_gate2_set_rate(struct clk_core *clk, ulong rate)
{
	struct clk_core *parent = clk->parent;

	if (parent)
		return parent->ops->set_rate(parent, rate);

	return -ENODEV;
}

const struct clk_core_ops clk_gate2_ops = {
	.set_rate = clk_gate2_set_rate,
	.enable = clk_gate2_enable,
	.disable = clk_gate2_disable,
	.get_rate = clk_generic_get_rate,
};
