// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright 2019 NXP
 * Copyright 2022 Purism
 * Peng Fan <peng.fan@nxp.com>
 */

#include <common.h>
#include <clk.h>
#include <clk-uclass.h>
#include <dm.h>
#include <log.h>
#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <dt-bindings/clock/imx8mq-clock.h>

#include "clk.h"

static const char *const pll_ref_sels[] = { "clock-osc-25m", "clock-osc-27m", "clock-phy-27m", "dummy", };
static const char *const arm_pll_bypass_sels[] = {"arm_pll", "arm_pll_ref_sel", };
static const char *const gpu_pll_bypass_sels[] = {"gpu_pll", "gpu_pll_ref_sel", };
static const char *const vpu_pll_bypass_sels[] = {"vpu_pll", "vpu_pll_ref_sel", };
static const char *const audio_pll1_bypass_sels[] = {"audio_pll1", "audio_pll1_ref_sel", };
static const char *const audio_pll2_bypass_sels[] = {"audio_pll2", "audio_pll2_ref_sel", };
static const char *const video_pll1_bypass_sels[] = {"video_pll1", "video_pll1_ref_sel", };

static const char *const imx8mq_a53_core_sels[] = {"arm_a53_div", "arm_pll_out", };
static const char *const imx8mq_a53_sels[] = {"clock-osc-25m", "arm_pll_out", "sys_pll2_500m",
					      "sys_pll2_1000m", "sys_pll1_800m", "sys_pll1_400m",
					      "audio_pll1_out", "sys_pll3_out", };

static const char *const imx8mq_ahb_sels[] = {"clock-osc-25m", "sys_pll1_133m", "sys_pll1_800m",
					      "sys_pll1_400m", "sys_pll2_125m", "sys_pll3_out",
					      "audio_pll1_out", "video_pll1_out", };

static const char *const imx8mq_dram_alt_sels[] = {"osc_25m", "sys_pll1_800m", "sys_pll1_100m",
						   "sys_pll2_500m", "sys_pll2_250m",
						   "sys_pll1_400m", "audio_pll1_out", "sys_pll1_266m", }  ;

static const char * const imx8mq_dram_apb_sels[] = {"osc_25m", "sys_pll2_200m", "sys_pll1_40m",
						    "sys_pll1_160m", "sys_pll1_800m", "sys_pll3_out",
						    "sys_pll2_250m", "audio_pll2_out", };

static const char *const imx8mq_enet_axi_sels[] = {"clock-osc-25m", "sys_pll1_266m", "sys_pll1_800m",
						   "sys_pll2_250m", "sys_pll2_200m", "audio_pll1_out",
						   "video_pll1_out", "sys_pll3_out", };

static const char *const imx8mq_enet_ref_sels[] = {"clock-osc-25m", "sys_pll2_125m", "sys_pll2_50m",
						   "sys_pll2_100m", "sys_pll1_160m", "audio_pll1_out",
						   "video_pll1_out", "clk_ext4", };

static const char *const imx8mq_enet_timer_sels[] = {"clock-osc-25m", "sys_pll2_100m", "audio_pll1_out",
						     "clk_ext1", "clk_ext2", "clk_ext3", "clk_ext4",
						     "video_pll1_out", };

static const char *const imx8mq_enet_phy_sels[] = {"clock-osc-25m", "sys_pll2_50m", "sys_pll2_125m",
						   "sys_pll2_200m", "sys_pll2_500m", "video_pll1_out",
						   "audio_pll2_out", };

static const char *const imx8mq_nand_usdhc_sels[] = {"clock-osc-25m", "sys_pll1_266m", "sys_pll1_800m",
						     "sys_pll2_200m", "sys_pll1_133m", "sys_pll3_out",
						     "sys_pll2_250m", "audio_pll1_out", };

static const char *const imx8mq_usb_bus_sels[] = {"clock-osc-25m", "sys_pll2_500m", "sys_pll1_800m",
						  "sys_pll2_100m", "sys_pll2_200m", "clk_ext2",
						  "clk_ext4", "audio_pll2_out", };

static const char *const imx8mq_usdhc1_sels[] = {"clock-osc-25m", "sys_pll1_400m", "sys_pll1_800m",
						 "sys_pll2_500m", "sys_pll3_out", "sys_pll1_266m",
						 "audio_pll2_out", "sys_pll1_100m", };

static const char *const imx8mq_usdhc2_sels[] = {"clock-osc-25m", "sys_pll1_400m", "sys_pll1_800m",
						 "sys_pll2_500m", "sys_pll3_out", "sys_pll1_266m",
						 "audio_pll2_out", "sys_pll1_100m", };

static const char *const imx8mq_i2c1_sels[] = {"clock-osc-25m", "sys_pll1_160m", "sys_pll2_50m",
					       "sys_pll3_out", "audio_pll1_out", "video_pll1_out",
					       "audio_pll2_out", "sys_pll1_133m", };

static const char *const imx8mq_i2c2_sels[] = {"clock-osc-25m", "sys_pll1_160m", "sys_pll2_50m",
					       "sys_pll3_out", "audio_pll1_out", "video_pll1_out",
					       "audio_pll2_out", "sys_pll1_133m", };

static const char *const imx8mq_i2c3_sels[] = {"clock-osc-25m", "sys_pll1_160m", "sys_pll2_50m",
					       "sys_pll3_out", "audio_pll1_out", "video_pll1_out",
					       "audio_pll2_out", "sys_pll1_133m", };

static const char *const imx8mq_i2c4_sels[] = {"clock-osc-25m", "sys_pll1_160m", "sys_pll2_50m",
					       "sys_pll3_out", "audio_pll1_out", "video_pll1_out",
					       "audio_pll2_out", "sys_pll1_133m", };

static const char *const imx8mq_uart1_sels[] = {"clock-osc-25m", "sys_pll1_80m", "sys_pll2_200m",
						"sys_pll2_100m", "sys_pll3_out", "clk_ext2",
						"clk_ext4", "audio_pll2_out", };

static const char *const imx8mq_uart2_sels[] = {"clock-osc-25m", "sys_pll1_80m", "sys_pll2_200m",
						"sys_pll2_100m", "sys_pll3_out", "clk_ext2",
						"clk_ext3", "audio_pll2_out", };

static const char *const imx8mq_uart3_sels[] = {"clock-osc-25m", "sys_pll1_80m", "sys_pll2_200m",
						"sys_pll2_100m", "sys_pll3_out", "clk_ext2",
						"clk_ext4", "audio_pll2_out", };

static const char *const imx8mq_uart4_sels[] = {"clock-osc-25m", "sys_pll1_80m", "sys_pll2_200m",
						"sys_pll2_100m", "sys_pll3_out", "clk_ext2",
						"clk_ext3", "audio_pll2_out", };

static const char *const imx8mq_wdog_sels[] = {"clock-osc-25m", "sys_pll1_133m", "sys_pll1_160m",
					       "vpu_pll_out", "sys_pll2_125m", "sys_pll3_out",
					       "sys_pll1_80m", "sys_pll2_166m", };

static const char *const imx8mq_qspi_sels[] = {"clock-osc-25m", "sys_pll1_400m", "sys_pll2_333m",
					       "sys_pll2_500m", "audio_pll2_out", "sys_pll1_266m",
					       "sys_pll3_out", "sys_pll1_100m", };

static const char *const imx8mq_usb_core_sels[] = {"clock-osc-25m", "sys_pll1_100m", "sys_pll1_40m",
						   "sys_pll2_100m", "sys_pll2_200m", "clk_ext2",
						   "clk_ext3", "audio_pll2_out", };

static const char *const imx8mq_usb_phy_sels[] = {"clock-osc-25m", "sys_pll1_100m", "sys_pll1_40m",
						  "sys_pll2_100m", "sys_pll2_200m", "clk_ext2",
						  "clk_ext3", "audio_pll2_out", };

static const char *const imx8mq_ecspi1_sels[] = {"clock-osc-25m", "sys_pll2_200m", "sys_pll1_40m",
						 "sys_pll1_160m", "sys_pll1_800m", "sys_pll3_out",
						 "sys_pll2_250m", "audio_pll2_out", };

static const char *const imx8mq_ecspi2_sels[] = {"clock-osc-25m", "sys_pll2_200m", "sys_pll1_40m",
						 "sys_pll1_160m", "sys_pll1_800m", "sys_pll3_out",
						 "sys_pll2_250m", "audio_pll2_out", };

static const char *const imx8mq_ecspi3_sels[] = {"clock-osc-25m", "sys_pll2_200m", "sys_pll1_40m",
						 "sys_pll1_160m", "sys_pll1_800m", "sys_pll3_out",
						 "sys_pll2_250m", "audio_pll2_out", };

static const char *const imx8mq_dram_core_sels[] = {"dram_pll_out", "dram_alt_root", };

static const char *const pllout_monitor_sels[] = {"clock-osc-25m", "clock-osc-27m", "clock-phy-27m",
						  "dummy", "clock-ckil", "audio_pll1_out_monitor",
						  "audio_pll2_out_monitor", "gpu_pll_out_monitor",
						  "vpu_pll_out_monitor", "video_pll1_out_monitor",
						  "arm_pll_out_monitor", "sys_pll1_out_monitor",
						  "sys_pll2_out_monitor", "sys_pll3_out_monitor",
						  "video_pll2_out_monitor", "dram_pll_out_monitor", };

struct clk_fixed_rate ckil_clk = imx_clk_fixed_rate(IMX8MQ_CLK_32K, "ckil", 32768);
struct clk_fixed_rate clock_osc_27m_clk = imx_clk_fixed_rate(IMX8MQ_CLK_27M, "clock-osc-27m", 27000000);

struct clk_mux dram_pll_ref_sel_clk = imx_clk_mux(IMX8MQ_DRAM_PLL1_REF_SEL, "dram_pll_ref_sel", 0x60, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux arm_pll_ref_sel_clk = imx_clk_mux(IMX8MQ_ARM_PLL_REF_SEL, "arm_pll_ref_sel", 0x28, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux gpu_pll_ref_sel_clk = imx_clk_mux(IMX8MQ_GPU_PLL_REF_SEL, "gpu_pll_ref_sel", 0x18, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux vpu_pll_ref_sel_clk = imx_clk_mux(IMX8MQ_VPU_PLL_REF_SEL, "vpu_pll_ref_sel", 0x20, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux sys3_pll_ref_sel_clk = imx_clk_mux(IMX8MQ_SYS3_PLL1_REF_SEL, "sys3_pll_ref_sel", 0x48, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux audio_pll1_ref_sel_clk = imx_clk_mux(IMX8MQ_AUDIO_PLL1_REF_SEL, "audio_pll1_ref_sel", 0x0, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux audio_pll2_ref_sel_clk = imx_clk_mux(IMX8MQ_AUDIO_PLL2_REF_SEL, "audio_pll2_ref_sel", 0x8, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux video_pll1_ref_sel_clk = imx_clk_mux(IMX8MQ_VIDEO_PLL1_REF_SEL, "video_pll1_ref_sel", 0x10, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));
struct clk_mux video_pll2_ref_sel_clk = imx_clk_mux(IMX8MQ_VIDEO2_PLL1_REF_SEL, "video_pll2_ref_sel", 0x54, 0, 2,
			   pll_ref_sels, ARRAY_SIZE(pll_ref_sels));

struct clk_pll14xx arm_pll_clk = imx_clk_pll14xx(IMX8MQ_ARM_PLL, "arm_pll", &arm_pll_ref_sel_clk.clk,
			       0x28, &imx_1416x_pll, &clk_pll1416x_ops);
struct clk_pll14xx gpu_pll_clk = imx_clk_pll14xx(IMX8MQ_GPU_PLL, "gpu_pll", &gpu_pll_ref_sel_clk.clk,
			       0x18, &imx_1416x_pll, &clk_pll1416x_ops);
struct clk_pll14xx vpu_pll_clk = imx_clk_pll14xx(IMX8MQ_VPU_PLL, "vpu_pll", &vpu_pll_ref_sel_clk.clk,
			       0x20, &imx_1416x_pll, &clk_pll1416x_ops);

struct clk_fixed_rate sys1_pll_clk = imx_clk_fixed_rate(IMX8MQ_SYS1_PLL1, "sys1_pll", 800000000);
struct clk_fixed_rate sys2_pll_clk = imx_clk_fixed_rate(IMX8MQ_SYS2_PLL1, "sys2_pll", 1000000000);
struct clk_fixed_rate sys3_pll_clk = imx_clk_fixed_rate(IMX8MQ_SYS2_PLL1, "sys3_pll", 1000000000);
struct clk_pll14xx audio_pll1_clk = imx_clk_pll14xx(IMX8MQ_AUDIO_PLL1, "audio_pll1", &audio_pll1_ref_sel_clk.clk,
			       0x0, &imx_1443x_pll, &clk_pll1443x_ops);
struct clk_pll14xx audio_pll2_clk = imx_clk_pll14xx(IMX8MQ_AUDIO_PLL2, "audio_pll2", &audio_pll2_ref_sel_clk.clk,
			       0x8, &imx_1443x_pll, &clk_pll1443x_ops);
struct clk_pll14xx video_pll1_clk = imx_clk_pll14xx(IMX8MQ_VIDEO_PLL1, "video_pll1", &video_pll1_ref_sel_clk.clk,
			       0x10, &imx_1443x_pll, &clk_pll1443x_ops);

/* PLL bypass out */
struct clk_mux arm_pll_bypass_clk = imx_clk_mux_flags(IMX8MQ_ARM_PLL_BYPASS, "arm_pll_bypass", 0x28, 4, 1,
				 arm_pll_bypass_sels,
				 ARRAY_SIZE(arm_pll_bypass_sels),
				 CLK_SET_RATE_PARENT);
struct clk_mux gpu_pll_bypass_clk = imx_clk_mux_flags(IMX8MQ_GPU_PLL_BYPASS, "gpu_pll_bypass", 0x18, 4, 1,
				 gpu_pll_bypass_sels,
				 ARRAY_SIZE(gpu_pll_bypass_sels),
				 CLK_SET_RATE_PARENT);
struct clk_mux vpu_pll_bypass_clk = imx_clk_mux_flags(IMX8MQ_VPU_PLL_BYPASS, "vpu_pll_bypass", 0x20, 4, 1,
				 vpu_pll_bypass_sels,
				 ARRAY_SIZE(vpu_pll_bypass_sels),
				 CLK_SET_RATE_PARENT);
struct clk_mux audio_pll1_bypass_clk = imx_clk_mux_flags(IMX8MQ_AUDIO_PLL1_BYPASS, "audio_pll1_bypass", 0x0, 4, 1,
				 audio_pll1_bypass_sels,
				 ARRAY_SIZE(audio_pll1_bypass_sels),
				 CLK_SET_RATE_PARENT);
struct clk_mux audio_pll2_bypass_clk = imx_clk_mux_flags(IMX8MQ_AUDIO_PLL2_BYPASS, "audio_pll2_bypass", 0x8, 4, 1,
				 audio_pll2_bypass_sels,
				 ARRAY_SIZE(audio_pll2_bypass_sels),
				 CLK_SET_RATE_PARENT);
struct clk_mux video_pll1_bypass_clk = imx_clk_mux_flags(IMX8MQ_VIDEO_PLL1_BYPASS, "video_pll1_bypass", 0x10, 4, 1,
				 video_pll1_bypass_sels,
				 ARRAY_SIZE(video_pll1_bypass_sels),
				 CLK_SET_RATE_PARENT);

/* PLL out gate */
struct clk_gate dram_pll_out_clk = imx_clk_gate(IMX8MQ_DRAM_PLL_OUT, "dram_pll_out", &dram_pll_ref_sel_clk.clk,
			    0x60, 13);
struct clk_gate arm_pll_out_clk = imx_clk_gate(IMX8MQ_ARM_PLL_OUT, "arm_pll_out", &arm_pll_bypass_clk.clk,
			    0x28, 11);
struct clk_gate gpu_pll_out_clk = imx_clk_gate(IMX8MQ_GPU_PLL_OUT, "gpu_pll_out", &gpu_pll_bypass_clk.clk,
			    0x18, 11);
struct clk_gate vpu_pll_out_clk = imx_clk_gate(IMX8MQ_VPU_PLL_OUT, "vpu_pll_out", &vpu_pll_bypass_clk.clk,
			    0x20, 11);
struct clk_gate audio_pll1_out_clk = imx_clk_gate(IMX8MQ_AUDIO_PLL1_OUT, "audio_pll1_out", &audio_pll1_bypass_clk.clk,
			    0x0, 11);
struct clk_gate audio_pll2_out_clk = imx_clk_gate(IMX8MQ_AUDIO_PLL2_OUT, "audio_pll2_out", &audio_pll2_bypass_clk.clk,
			    0x8, 11);
struct clk_gate video_pll1_out_clk = imx_clk_gate(IMX8MQ_VIDEO_PLL1_OUT, "video_pll1_out", &video_pll1_bypass_clk.clk,
			    0x10, 11);

struct clk_gate sys_pll1_out_clk = imx_clk_gate(IMX8MQ_SYS1_PLL_OUT, "sys_pll1_out", &sys1_pll_clk.clk,
			    0x30, 11);
struct clk_gate sys_pll2_out_clk = imx_clk_gate(IMX8MQ_SYS2_PLL_OUT, "sys_pll2_out", &sys2_pll_clk.clk,
			    0x3c, 11);
struct clk_gate sys_pll3_out_clk = imx_clk_gate(IMX8MQ_SYS3_PLL_OUT, "sys_pll3_out", &sys3_pll_clk.clk,
			    0x48, 11);
struct clk_gate video_pll2_out_clk = imx_clk_gate(IMX8MQ_VIDEO2_PLL_OUT, "video_pll2_out", &video_pll2_ref_sel_clk.clk,
			    0x54, 11);

/* SYS PLL fixed output */
struct clk_fixed_factor sys_pll1_40m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_40M, "sys_pll1_40m", &sys_pll1_out_clk.clk, 1, 20);
struct clk_fixed_factor sys_pll1_80m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_80M, "sys_pll1_80m", &sys_pll1_out_clk.clk, 1, 10);
struct clk_fixed_factor sys_pll1_100m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_100M, "sys_pll1_100m", &sys_pll1_out_clk.clk, 1, 8);
struct clk_fixed_factor sys_pll1_133m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_133M, "sys_pll1_133m", &sys_pll1_out_clk.clk, 1, 6);
struct clk_fixed_factor sys_pll1_160m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_160M, "sys_pll1_160m", &sys_pll1_out_clk.clk, 1, 5);
struct clk_fixed_factor sys_pll1_200m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_200M, "sys_pll1_200m", &sys_pll1_out_clk.clk, 1, 4);
struct clk_fixed_factor sys_pll1_266m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_266M, "sys_pll1_266m", &sys_pll1_out_clk.clk, 1, 3);
struct clk_fixed_factor sys_pll1_400m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_400M, "sys_pll1_400m", &sys_pll1_out_clk.clk, 1, 2);
struct clk_fixed_factor sys_pll1_800m_clk = imx_clk_fixed_factor(IMX8MQ_SYS1_PLL_800M, "sys_pll1_800m", &sys_pll1_out_clk.clk, 1, 1);

struct clk_fixed_factor sys_pll2_50m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_50M, "sys_pll2_50m", &sys_pll2_out_clk.clk, 1, 20);
struct clk_fixed_factor sys_pll2_100m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_100M, "sys_pll2_100m", &sys_pll2_out_clk.clk, 1, 10);
struct clk_fixed_factor sys_pll2_125m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_125M, "sys_pll2_125m", &sys_pll2_out_clk.clk, 1, 8);
struct clk_fixed_factor sys_pll2_166m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_166M, "sys_pll2_166m", &sys_pll2_out_clk.clk, 1, 6);
struct clk_fixed_factor sys_pll2_200m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_200M, "sys_pll2_200m", &sys_pll2_out_clk.clk, 1, 5);
struct clk_fixed_factor sys_pll2_250m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_250M, "sys_pll2_250m", &sys_pll2_out_clk.clk, 1, 4);
struct clk_fixed_factor sys_pll2_333m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_333M, "sys_pll2_333m", &sys_pll2_out_clk.clk, 1, 3);
struct clk_fixed_factor sys_pll2_500m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_500M, "sys_pll2_500m", &sys_pll2_out_clk.clk, 1, 2);
struct clk_fixed_factor sys_pll2_1000m_clk = imx_clk_fixed_factor(IMX8MQ_SYS2_PLL_1000M, "sys_pll2_1000m", &sys_pll2_out_clk.clk, 1, 1);

struct clk_divider audio_pll1_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_AUDIO_PLL1_DIV, "audio_pll1_out_monitor", &audio_pll1_bypass_clk.clk, 0x78, 0, 3);
struct clk_divider audio_pll2_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_AUDIO_PLL2_DIV, "audio_pll2_out_monitor", &audio_pll2_bypass_clk.clk, 0x78, 4, 3);
struct clk_divider video_pll1_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_VIDEO_PLL1_DIV, "video_pll1_out_monitor", &video_pll1_bypass_clk.clk, 0x78, 8, 3);
struct clk_divider gpu_pll_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_GPU_PLL_DIV, "gpu_pll_out_monitor", &gpu_pll_bypass_clk.clk, 0x78, 12, 3);
struct clk_divider vpu_pll_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_VPU_PLL_DIV, "vpu_pll_out_monitor", &vpu_pll_bypass_clk.clk, 0x78, 16, 3);
struct clk_divider arm_pll_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_ARM_PLL_DIV, "arm_pll_out_monitor", &arm_pll_bypass_clk.clk, 0x78, 20, 3);
struct clk_divider sys_pll1_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_SYS_PLL1_DIV, "sys_pll1_out_monitor", &sys_pll1_out_clk.clk, 0x7c, 0, 3);
struct clk_divider sys_pll2_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_SYS_PLL2_DIV, "sys_pll2_out_monitor", &sys_pll2_out_clk.clk, 0x7c, 4, 3);
struct clk_divider sys_pll3_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_SYS_PLL3_DIV, "sys_pll3_out_monitor", &sys_pll3_out_clk.clk, 0x7c, 8, 3);
struct clk_divider dram_pll_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_DRAM_PLL_DIV, "dram_pll_out_monitor", &dram_pll_out_clk.clk, 0x7c, 12, 3);
struct clk_divider video_pll2_out_monitor_clk = imx_clk_divider(IMX8MQ_CLK_MON_VIDEO_PLL2_DIV, "video_pll2_out_monitor", &video_pll2_out_clk.clk, 0x7c, 16, 3);
struct clk_mux pllout_monitor_sel_clk = imx_clk_mux_flags(IMX8MQ_CLK_MON_SEL, "pllout_monitor_sel", 0x74, 0, 4,
				 pllout_monitor_sels,
				 ARRAY_SIZE(pllout_monitor_sels),
				 CLK_SET_RATE_PARENT);
struct clk_gate2 pllout_monitor_clk2_clk = imx_clk_gate4(IMX8MQ_CLK_MON_CLK2_OUT, "pllout_monitor_clk2", &pllout_monitor_sel_clk.clk, 0x74, 4);

struct clk_mux arm_a53_src_clk = imx_clk_mux2(IMX8MQ_CLK_A53_SRC, "arm_a53_src", 0x8000, 24, 3,
			    imx8mq_a53_sels, ARRAY_SIZE(imx8mq_a53_sels));
struct clk_gate arm_a53_cg_clk = imx_clk_gate3(IMX8MQ_CLK_A53_CG, "arm_a53_cg", &arm_a53_src_clk.clk, 0x8000, 28);
struct clk_divider arm_a53_div_clk = imx_clk_divider2(IMX8MQ_CLK_A53_DIV, "arm_a53_div", &arm_a53_cg_clk.clk,
				0x8000, 0, 3);
struct clk_mux arm_a53_core_clk = imx_clk_mux2(IMX8MQ_CLK_A53_CORE, "arm_a53_core", 0x9880, 24, 1,
			    imx8mq_a53_core_sels, ARRAY_SIZE(imx8mq_a53_core_sels));

struct clk_composite ahb_clk = imx8m_clk_composite_critical(IMX8MQ_CLK_AHB, "ahb", imx8mq_ahb_sels,
					    0x9000);
struct clk_divider ipg_root_clk = imx_clk_divider2(IMX8MQ_CLK_IPG_ROOT, "ipg_root", &ahb_clk.clk, 0x9080, 0, 1);

struct clk_composite enet_axi_clk = imx8m_clk_composite(IMX8MQ_CLK_ENET_AXI, "enet_axi", imx8mq_enet_axi_sels,
				   0x8880);
struct clk_composite nand_usdhc_bus = imx8m_clk_composite_critical(IMX8MQ_CLK_NAND_USDHC_BUS, "nand_usdhc_bus",
					    imx8mq_nand_usdhc_sels,
					    0x8900);
struct clk_composite usb_bus_clk = imx8m_clk_composite(IMX8MQ_CLK_USB_BUS, "usb_bus", imx8mq_usb_bus_sels, 0x8b80);

/* DRAM */
struct clk_mux dram_core_clk_clk = imx_clk_mux2(IMX8MQ_CLK_DRAM_CORE, "dram_core_clk", 0x9800, 24, 1,
			    imx8mq_dram_core_sels, ARRAY_SIZE(imx8mq_dram_core_sels));
struct clk_composite dram_alt_clk = imx8m_clk_composite(IMX8MQ_CLK_DRAM_ALT, "dram_alt", imx8mq_dram_alt_sels, 0xa000);
struct clk_composite dram_apb_clk = imx8m_clk_composite_critical(IMX8MQ_CLK_DRAM_APB, "dram_apb", imx8mq_dram_apb_sels, 0xa080);

/* IP */
struct clk_composite usdhc1_clk = imx8m_clk_composite(IMX8MQ_CLK_USDHC1, "usdhc1", imx8mq_usdhc1_sels,
				   0xac00);
struct clk_composite usdhc2_clk = imx8m_clk_composite(IMX8MQ_CLK_USDHC2, "usdhc2", imx8mq_usdhc2_sels,
				   0xac80);
struct clk_composite i2c1_clk = imx8m_clk_composite(IMX8MQ_CLK_I2C1, "i2c1", imx8mq_i2c1_sels, 0xad00);
struct clk_composite i2c2_clk = imx8m_clk_composite(IMX8MQ_CLK_I2C2, "i2c2", imx8mq_i2c2_sels, 0xad80);
struct clk_composite i2c3_clk = imx8m_clk_composite(IMX8MQ_CLK_I2C3, "i2c3", imx8mq_i2c3_sels, 0xae00);
struct clk_composite i2c4_clk = imx8m_clk_composite(IMX8MQ_CLK_I2C4, "i2c4", imx8mq_i2c4_sels, 0xae80);
struct clk_composite wdog_clk = imx8m_clk_composite(IMX8MQ_CLK_WDOG, "wdog", imx8mq_wdog_sels, 0xb900);
struct clk_composite uart1_clk = imx8m_clk_composite(IMX8MQ_CLK_UART1, "uart1", imx8mq_uart1_sels, 0xaf00);
struct clk_composite uart2_clk = imx8m_clk_composite(IMX8MQ_CLK_UART2, "uart2", imx8mq_uart2_sels, 0xaf80);
struct clk_composite uart3_clk = imx8m_clk_composite(IMX8MQ_CLK_UART3, "uart3", imx8mq_uart3_sels, 0xb000);
struct clk_composite uart4_clk = imx8m_clk_composite(IMX8MQ_CLK_UART4, "uart4", imx8mq_uart4_sels, 0xb080);
struct clk_composite qspi_clk = imx8m_clk_composite(IMX8MQ_CLK_QSPI, "qspi", imx8mq_qspi_sels, 0xab80);
struct clk_composite usb_core_ref_clk = imx8m_clk_composite(IMX8MQ_CLK_USB_CORE_REF, "usb_core_ref", imx8mq_usb_core_sels, 0xb100);
struct clk_composite usb_phy_ref_clk = imx8m_clk_composite(IMX8MQ_CLK_USB_PHY_REF, "usb_phy_ref", imx8mq_usb_phy_sels, 0xb180);
struct clk_composite ecspi1_clk = imx8m_clk_composite(IMX8MQ_CLK_ECSPI1, "ecspi1", imx8mq_ecspi1_sels, 0xb280);
struct clk_composite ecspi2_clk = imx8m_clk_composite(IMX8MQ_CLK_ECSPI2, "ecspi2", imx8mq_ecspi2_sels, 0xb300);
struct clk_composite ecspi3_clk = imx8m_clk_composite(IMX8MQ_CLK_ECSPI3, "ecspi3", imx8mq_ecspi3_sels, 0xc180);

struct clk_gate2 ecspi1_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_ECSPI1_ROOT, "ecspi1_root_clk", &ecspi1_clk.clk, 0x4070, 0);
struct clk_gate2 ecspi2_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_ECSPI2_ROOT, "ecspi2_root_clk", &ecspi2_clk.clk, 0x4080, 0);
struct clk_gate2 ecspi3_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_ECSPI3_ROOT, "ecspi3_root_clk", &ecspi3_clk.clk, 0x4090, 0);
struct clk_gate2 i2c1_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_I2C1_ROOT, "i2c1_root_clk", &i2c1_clk.clk, 0x4170, 0);
struct clk_gate2 i2c2_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_I2C2_ROOT, "i2c2_root_clk", &i2c2_clk.clk, 0x4180, 0);
struct clk_gate2 i2c3_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_I2C3_ROOT, "i2c3_root_clk", &i2c3_clk.clk, 0x4190, 0);
struct clk_gate2 i2c4_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_I2C4_ROOT, "i2c4_root_clk", &i2c4_clk.clk, 0x41a0, 0);
struct clk_gate2 uart1_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_UART1_ROOT, "uart1_root_clk", &uart1_clk.clk, 0x4490, 0);
struct clk_gate2 uart2_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_UART2_ROOT, "uart2_root_clk", &uart2_clk.clk, 0x44a0, 0);
struct clk_gate2 uart3_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_UART3_ROOT, "uart3_root_clk", &uart3_clk.clk, 0x44b0, 0);
struct clk_gate2 uart4_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_UART4_ROOT, "uart4_root_clk", &uart4_clk.clk, 0x44c0, 0);
struct clk_gate2 ocotp_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_OCOTP_ROOT, "ocotp_root_clk", &ipg_root_clk.clk, 0x4220, 0);
struct clk_gate2 usdhc1_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_USDHC1_ROOT, "usdhc1_root_clk", &usdhc1_clk.clk, 0x4510, 0);
struct clk_gate2 usdhc2_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_USDHC2_ROOT, "usdhc2_root_clk", &usdhc2_clk.clk, 0x4520, 0);
struct clk_gate2 wdog1_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_WDOG1_ROOT, "wdog1_root_clk", &wdog_clk.clk, 0x4530, 0);
struct clk_gate2 wdog2_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_WDOG2_ROOT, "wdog2_root_clk", &wdog_clk.clk, 0x4540, 0);
struct clk_gate2 wdog3_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_WDOG3_ROOT, "wdog3_root_clk", &wdog_clk.clk, 0x4550, 0);
struct clk_gate2 qspi_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_QSPI_ROOT, "qspi_root_clk", &qspi_clk.clk, 0x42f0, 0);
struct clk_gate2 usb1_ctrl_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_USB1_CTRL_ROOT, "usb1_ctrl_root_clk", &usb_bus_clk.clk, 0x44d0, 0);
struct clk_gate2 usb2_ctrl_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_USB2_CTRL_ROOT, "usb2_ctrl_root_clk", &usb_bus_clk.clk, 0x44e0, 0);
struct clk_gate2 usb1_phy_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_USB1_PHY_ROOT, "usb1_phy_root_clk", &usb_phy_ref_clk.clk, 0x44f0, 0);
struct clk_gate2 usb2_phy_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_USB2_PHY_ROOT, "usb2_phy_root_clk", &usb_phy_ref_clk.clk, 0x4500, 0);

struct clk_composite enet_ref_clk = imx8m_clk_composite(IMX8MQ_CLK_ENET_REF, "enet_ref", imx8mq_enet_ref_sels,
				   0xa980);
struct clk_composite enet_timer_clk = imx8m_clk_composite(IMX8MQ_CLK_ENET_TIMER, "enet_timer", imx8mq_enet_timer_sels,
				   0xaa00);
struct clk_composite enet_phy_clk = imx8m_clk_composite(IMX8MQ_CLK_ENET_PHY_REF, "enet_phy", imx8mq_enet_phy_sels,
				   0xaa80);
struct clk_gate2 enet1_root_clk_clk = imx_clk_gate4(IMX8MQ_CLK_ENET1_ROOT, "enet1_root_clk", &enet_axi_clk.clk,
			     0x40a0, 0);

struct clk_fixed_factor dram_alt_root_clk = imx_clk_fixed_factor(IMX8MQ_CLK_DRAM_ALT_ROOT, "dram_alt_root", &dram_alt_clk.clk, 1, 4);

struct clk_core *imx8mq_clks[] = {
	&ckil_clk.clk,
	&clock_osc_27m_clk.clk,

	&dram_pll_ref_sel_clk.clk,
	&arm_pll_ref_sel_clk.clk,
	&gpu_pll_ref_sel_clk.clk,
	&vpu_pll_ref_sel_clk.clk,
	&sys3_pll_ref_sel_clk.clk,
	&audio_pll1_ref_sel_clk.clk,
	&audio_pll2_ref_sel_clk.clk,
	&video_pll1_ref_sel_clk.clk,
	&video_pll2_ref_sel_clk.clk,

	&arm_pll_clk.clk,
	&gpu_pll_clk.clk,
	&vpu_pll_clk.clk,

	&sys1_pll_clk.clk,
	&sys2_pll_clk.clk,
	&sys3_pll_clk.clk,
	&audio_pll1_clk.clk,
	&audio_pll2_clk.clk,
	&video_pll1_clk.clk,

	&arm_pll_bypass_clk.clk,
	&gpu_pll_bypass_clk.clk,
	&vpu_pll_bypass_clk.clk,
	&audio_pll1_bypass_clk.clk,
	&audio_pll2_bypass_clk.clk,
	&video_pll1_bypass_clk.clk,

	&dram_pll_out_clk.clk,
	&arm_pll_out_clk.clk,
	&gpu_pll_out_clk.clk,
	&vpu_pll_out_clk.clk,
	&audio_pll1_out_clk.clk,
	&audio_pll2_out_clk.clk,
	&video_pll1_out_clk.clk,

	&sys_pll1_out_clk.clk,
	&sys_pll2_out_clk.clk,
	&sys_pll3_out_clk.clk,
	&video_pll2_out_clk.clk,

	&sys_pll1_40m_clk.clk,
	&sys_pll1_80m_clk.clk,
	&sys_pll1_100m_clk.clk,
	&sys_pll1_133m_clk.clk,
	&sys_pll1_160m_clk.clk,
	&sys_pll1_200m_clk.clk,
	&sys_pll1_266m_clk.clk,
	&sys_pll1_400m_clk.clk,
	&sys_pll1_800m_clk.clk,

	&sys_pll2_50m_clk.clk,
	&sys_pll2_100m_clk.clk,
	&sys_pll2_125m_clk.clk,
	&sys_pll2_166m_clk.clk,
	&sys_pll2_200m_clk.clk,
	&sys_pll2_250m_clk.clk,
	&sys_pll2_333m_clk.clk,
	&sys_pll2_500m_clk.clk,
	&sys_pll2_1000m_clk.clk,

	&audio_pll1_out_monitor_clk.clk,
	&audio_pll2_out_monitor_clk.clk,
	&video_pll1_out_monitor_clk.clk,
	&gpu_pll_out_monitor_clk.clk,
	&vpu_pll_out_monitor_clk.clk,
	&arm_pll_out_monitor_clk.clk,
	&sys_pll1_out_monitor_clk.clk,
	&sys_pll2_out_monitor_clk.clk,
	&sys_pll3_out_monitor_clk.clk,
	&dram_pll_out_monitor_clk.clk,
	&video_pll2_out_monitor_clk.clk,
	&pllout_monitor_sel_clk.clk,
	&pllout_monitor_clk2_clk.clk,

	&arm_a53_src_clk.clk,
	&arm_a53_cg_clk.clk,
	&arm_a53_div_clk.clk,
	&arm_a53_core_clk.clk,

	&ahb_clk.clk,
	&ipg_root_clk.clk,

	&enet_axi_clk.clk,
	&usb_bus_clk.clk,

	&dram_core_clk_clk.clk,
	&dram_alt_clk.clk,
	&dram_apb_clk.clk,

	&usdhc1_clk.clk,
	&usdhc2_clk.clk,
	&i2c1_clk.clk,
	&i2c2_clk.clk,
	&i2c3_clk.clk,
	&i2c4_clk.clk,
	&wdog_clk.clk,
	&uart1_clk.clk,
	&uart2_clk.clk,
	&uart3_clk.clk,
	&uart4_clk.clk,
	&qspi_clk.clk,
	&usb_core_ref_clk.clk,
	&usb_phy_ref_clk.clk,
	&ecspi1_clk.clk,
	&ecspi2_clk.clk,
	&ecspi3_clk.clk,

	&ecspi1_root_clk_clk.clk,
	&ecspi2_root_clk_clk.clk,
	&ecspi3_root_clk_clk.clk,
	&i2c1_root_clk_clk.clk,
	&i2c2_root_clk_clk.clk,
	&i2c3_root_clk_clk.clk,
	&i2c4_root_clk_clk.clk,
	&uart1_root_clk_clk.clk,
	&uart2_root_clk_clk.clk,
	&uart3_root_clk_clk.clk,
	&uart4_root_clk_clk.clk,
	&ocotp_root_clk_clk.clk,
	&usdhc1_root_clk_clk.clk,
	&usdhc2_root_clk_clk.clk,
	&wdog1_root_clk_clk.clk,
	&wdog2_root_clk_clk.clk,
	&wdog3_root_clk_clk.clk,
	&qspi_root_clk_clk.clk,
	&usb1_ctrl_root_clk_clk.clk,
	&usb2_ctrl_root_clk_clk.clk,
	&usb1_phy_root_clk_clk.clk,
	&usb2_phy_root_clk_clk.clk,

	&enet_ref_clk.clk,
	&enet_timer_clk.clk,
	&enet_phy_clk.clk,
	&enet1_root_clk_clk.clk,

	&dram_alt_root_clk.clk,
};

static int imx8mq_clk_probe(struct udevice *dev)
{
	void __iomem *base;

	/* add the base offset to clocks which expect an absolute address */

	base = (void *)ANATOP_BASE_ADDR;

	*(uintptr_t *)&dram_pll_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&arm_pll_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&gpu_pll_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&vpu_pll_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&sys3_pll_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll1_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll2_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll1_ref_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll2_ref_sel_clk.reg += (uintptr_t)base;

	*(uintptr_t *)&arm_pll_clk.base += (uintptr_t)base;
	*(uintptr_t *)&gpu_pll_clk.base += (uintptr_t)base;
	*(uintptr_t *)&vpu_pll_clk.base += (uintptr_t)base;

	*(uintptr_t *)&audio_pll1_clk.base += (uintptr_t)base;
	*(uintptr_t *)&audio_pll2_clk.base += (uintptr_t)base;
	*(uintptr_t *)&video_pll1_clk.base += (uintptr_t)base;

	*(uintptr_t *)&arm_pll_bypass_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&gpu_pll_bypass_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&vpu_pll_bypass_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll1_bypass_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll2_bypass_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll1_bypass_clk.reg += (uintptr_t)base;

	*(uintptr_t *)&dram_pll_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&arm_pll_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&gpu_pll_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&vpu_pll_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll1_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll2_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll1_out_clk.reg += (uintptr_t)base;

	*(uintptr_t *)&sys_pll1_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&sys_pll2_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&sys_pll3_out_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll2_out_clk.reg += (uintptr_t)base;

	*(uintptr_t *)&audio_pll1_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&audio_pll2_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll1_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&gpu_pll_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&vpu_pll_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&arm_pll_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&sys_pll1_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&sys_pll2_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&sys_pll3_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&dram_pll_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&video_pll2_out_monitor_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&pllout_monitor_sel_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&pllout_monitor_clk2_clk.reg += (uintptr_t)base;

	base = dev_read_addr_ptr(dev);
	if (!base) {
		printf("%s : base failed\n", __func__);
		return -EINVAL;
	}

	*(uintptr_t *)&arm_a53_src_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&arm_a53_cg_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&arm_a53_div_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&arm_a53_src_clk.reg += (uintptr_t)base;

	imx_8m_clk_composite_add_base(ahb_clk, base)
	*(uintptr_t *)&ipg_root_clk.reg += (uintptr_t)base;

	imx_8m_clk_composite_add_base(enet_axi_clk, base)
	imx_8m_clk_composite_add_base(usb_bus_clk, base)

	*(uintptr_t *)&dram_core_clk_clk.reg += (uintptr_t)base;
	imx_8m_clk_composite_add_base(dram_alt_clk, base)
	imx_8m_clk_composite_add_base(dram_apb_clk, base)

	imx_8m_clk_composite_add_base(usdhc1_clk, base)
	imx_8m_clk_composite_add_base(usdhc2_clk, base)
	imx_8m_clk_composite_add_base(i2c1_clk, base)
	imx_8m_clk_composite_add_base(i2c2_clk, base)
	imx_8m_clk_composite_add_base(i2c3_clk, base)
	imx_8m_clk_composite_add_base(i2c4_clk, base)
	imx_8m_clk_composite_add_base(wdog_clk, base)
	imx_8m_clk_composite_add_base(uart1_clk, base)
	imx_8m_clk_composite_add_base(uart2_clk, base)
	imx_8m_clk_composite_add_base(uart3_clk, base)
	imx_8m_clk_composite_add_base(uart4_clk, base)
	imx_8m_clk_composite_add_base(qspi_clk, base)
	imx_8m_clk_composite_add_base(usb_core_ref_clk, base)
	imx_8m_clk_composite_add_base(usb_phy_ref_clk, base)
	imx_8m_clk_composite_add_base(ecspi1_clk, base)
	imx_8m_clk_composite_add_base(ecspi2_clk, base)
	imx_8m_clk_composite_add_base(ecspi3_clk, base)

	*(uintptr_t *)&ecspi1_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&ecspi2_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&ecspi3_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&i2c1_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&i2c2_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&i2c3_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&i2c4_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&uart1_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&uart2_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&uart3_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&uart4_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&ocotp_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&usdhc1_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&usdhc2_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&wdog1_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&wdog2_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&wdog3_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&qspi_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&usb1_ctrl_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&usb2_ctrl_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&usb1_phy_root_clk_clk.reg += (uintptr_t)base;
	*(uintptr_t *)&usb2_phy_root_clk_clk.reg += (uintptr_t)base;

	imx_8m_clk_composite_add_base(enet_ref_clk, base)
	imx_8m_clk_composite_add_base(enet_timer_clk, base)
	imx_8m_clk_composite_add_base(enet_phy_clk, base)
	*(uintptr_t *)&enet1_root_clk_clk.reg += (uintptr_t)base;

	static struct clk_array clks = {
		.arr = imx8mq_clks,
		.count = ARRAY_SIZE(imx8mq_clks),
	};

	return clk_register(dev, &clks);
}

static const struct udevice_id imx8mq_clk_ids[] = {
	{ .compatible = "fsl,imx8mq-ccm" },
	{ },
};

U_BOOT_DRIVER(imx8mq_clk) = {
	.name = "clk_imx8mq",
	.id = UCLASS_CLK,
	.of_match = imx8mq_clk_ids,
	.ops = &ccf_clk_ops,
	.probe = imx8mq_clk_probe,
	.flags = DM_FLAG_PRE_RELOC,
};
