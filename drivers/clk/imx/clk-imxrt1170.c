// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2022
 * Author(s): Jesse Taube <Mr.Bossman075@gmail.com>
 */

#include <common.h>
#include <clk.h>
#include <clk-uclass.h>
#include <dm.h>
#include <log.h>
#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <dt-bindings/clock/imxrt1170-clock.h>

#include "clk.h"

static ulong imxrt1170_clk_get_rate(struct clk *clk)
{
	struct clk *c;
	int ret;

	debug("%s(#%lu)\n", __func__, clk->id);

	ret = clk_get_by_id(clk->id, &c);
	if (ret)
		return ret;

	return clk_get_rate(c);
}

static ulong imxrt1170_clk_set_rate(struct clk *clk, ulong rate)
{
	struct clk *c;
	int ret;

	debug("%s(#%lu), rate: %lu\n", __func__, clk->id, rate);

	ret = clk_get_by_id(clk->id, &c);
	if (ret)
		return ret;

	return clk_set_rate(c, rate);
}

static int __imxrt1170_clk_enable(struct clk *clk, bool enable)
{
	struct clk *c;
	int ret;

	debug("%s(#%lu) en: %d\n", __func__, clk->id, enable);

	ret = clk_get_by_id(clk->id, &c);
	if (ret)
		return ret;

	if (enable)
		ret = clk_enable(c);
	else
		ret = clk_disable(c);

	return ret;
}

static int imxrt1170_clk_disable(struct clk *clk)
{
	return __imxrt1170_clk_enable(clk, 0);
}

static int imxrt1170_clk_enable(struct clk *clk)
{
	return __imxrt1170_clk_enable(clk, 1);
}

static int imxrt1170_clk_set_parent(struct clk *clk, struct clk *parent)
{
	struct clk *c, *cp;
	int ret;

	debug("%s(#%lu), parent: %lu\n", __func__, clk->id, parent->id);

	ret = clk_get_by_id(clk->id, &c);
	if (ret)
		return ret;

	ret = clk_get_by_id(parent->id, &cp);
	if (ret)
		return ret;

	return clk_set_parent(c, cp);
}

static struct clk_ops imxrt1170_clk_ops = {
	.set_rate = imxrt1170_clk_set_rate,
	.get_rate = imxrt1170_clk_get_rate,
	.enable = imxrt1170_clk_enable,
	.disable = imxrt1170_clk_disable,
	.set_parent = imxrt1170_clk_set_parent,
};

static const char * const lpuart1_sels[] = {"rcosc48M_div2", "osc", "rcosc400M", "rcosc16M",
"pll3_div2", "pll1_div5", "pll2_sys", "pll2_pfd3"};
static const char * const gpt1_sels[] = {"rcosc48M_div2", "osc", "rcosc400M", "rcosc16M",
"pll3_div2", "pll1_div5", "pll3_pfd2", "pll3_pfd3"};
static const char * const usdhc1_sels[] = {"rcosc48M_div2", "osc", "rcosc400M", "rcosc16M",
"pll2_pfd2", "pll2_pfd0", "pll1_div5", "pll_arm"};
static const char * const semc_sels[] = {"rcosc48M_div2", "osc", "rcosc400M", "rcosc16M",
"pll1_div5", "pll2_sys", "pll2_pfd2", "pll3_pfd0"};

/* Anatop clocks */
struct clk_fixed_factor rcosc48M_clk = imx_clk_fixed_factor(IMXRT1170_CLK_RCOSC_48M, "rcosc48M", "rcosc16M", 3, 1);
struct clk_fixed_factor rcosc400M_clk = imx_clk_fixed_factor(IMXRT1170_CLK_RCOSC_400M, "rcosc400M",  "rcosc16M", 25, 1);
struct clk_fixed_factor rcosc48M_div2_clk = imx_clk_fixed_factor(IMXRT1170_CLK_RCOSC_48M_DIV2, "rcosc48M_div2",  "rcosc48M", 1, 2);


struct clk_pllv3 pll_arm = imx_clk_pllv3(IMXRT1170_CLK_PLL_ARM, IMX_PLLV3_SYS, "pll_arm", "osc", 0x200, 0xff);
struct clk_pllv3 pll3_sys = imx_clk_pllv3(IMXRT1170_CLK_PLL3, IMX_PLLV3_GENERICV2, "pll3_sys", "osc", 0x210, 1));
struct clk_pllv3 pll2_sys = imx_clk_pllv3(IMXRT1170_CLK_PLL2, IMX_PLLV3_GENERICV2, "pll2_sys", "osc", 0x240, 1));

struct clk_pfd pll3_pfd0_clk = imx_clk_pfd(IMXRT1170_CLK_PLL3_PFD0, "pll3_pfd0", "pll3_sys", 0x230, 0);
struct clk_pfd pll3_pfd1_clk = imx_clk_pfd(IMXRT1170_CLK_PLL3_PFD1, "pll3_pfd1", "pll3_sys", 0x230, 1);
struct clk_pfd pll3_pfd2_clk = imx_clk_pfd(IMXRT1170_CLK_PLL3_PFD2, "pll3_pfd2", "pll3_sys", 0x230, 2);
struct clk_pfd pll3_pfd3_clk = imx_clk_pfd(IMXRT1170_CLK_PLL3_PFD3, "pll3_pfd3", "pll3_sys", 0x230, 3);

struct clk_pfd pll2_pfd0_clk = imx_clk_pfd(IMXRT1170_CLK_PLL2_PFD0, "pll2_pfd0", "pll2_sys", 0x270, 0);
struct clk_pfd pll2_pfd1_clk = imx_clk_pfd(IMXRT1170_CLK_PLL2_PFD1, "pll2_pfd1", "pll2_sys", 0x270, 1);
struct clk_pfd pll2_pfd2_clk = imx_clk_pfd(IMXRT1170_CLK_PLL2_PFD2, "pll2_pfd2", "pll2_sys", 0x270, 2);
struct clk_pfd pll2_pfd3_clk = imx_clk_pfd(IMXRT1170_CLK_PLL2_PFD3, "pll2_pfd3", "pll2_sys", 0x270, 3);

struct clk_fixed_factor pll3_div2_clk = imx_clk_fixed_factor(IMXRT1170_CLK_PLL3_DIV2, "pll3_div2", "pll3_sys", 1, 2);

/* CCM clocks */
struct clk_mux lpuart1_sel_clk = imx_clk_mux(IMXRT1170_CLK_LPUART1_SEL, "lpuart1_sel",
				 (25 * 0x80), 8, 3, lpuart1_sels, ARRAY_SIZE(lpuart1_sels));
struct clk_divider lpuart1_clk = imx_clk_divider(IMXRT1170_CLK_LPUART1, "lpuart1", "lpuart1_sel",
				 (25 * 0x80), 0, 8));

struct clk_mux usdhc1_sel_clk = imx_clk_mux(IMXRT1170_CLK_USDHC1_SEL, "usdhc1_sel",
				(58 * 0x80), 8, 3, usdhc1_sels, ARRAY_SIZE(usdhc1_sels));
struct clk_divider usdhc1_clk = imx_clk_divider(IMXRT1170_CLK_USDHC1, "usdhc1", "usdhc1_sel",
				(58 * 0x80), 0, 8);

struct clk_mux gpt1_sel_clk = imx_clk_mux(IMXRT1170_CLK_GPT1_SEL, "gpt1_sel",
			      (14 * 0x80), 8, 3, gpt1_sels, ARRAY_SIZE(gpt1_sels)));
struct clk_divider gpt1_clk = imx_clk_divider(IMXRT1170_CLK_GPT1, "gpt1", "gpt1_sel",
			      (14 * 0x80), 0, 8);

struct clk_mux semc_sel_clk = imx_clk_mux(IMXRT1170_CLK_SEMC_SEL, "semc_sel",
			      (4 * 0x80), 8, 3, semc_sels, ARRAY_SIZE(semc_sels));
struct clk_divider semc_clk = imx_clk_divider(IMXRT1170_CLK_SEMC, "semc", "semc_sel",
			      (4 * 0x80), 0, 8);


static struct clk_core *imxrt1170_clks[] = {
	/* Anatop clocks */
	&rcosc48M_clk.clk,
	&rcosc400M_clk.clk,
	&rcosc48M_div2_clk.clk,
	&pll_arm.clk,
	&pll3_sys.clk,
	&pll2_sys.clk,
	&pll3_pfd0_clk.clk,
	&pll3_pfd1_clk.clk,
	&pll3_pfd2_clk.clk,
	&pll3_pfd3_clk.clk,
	&pll2_pfd0_clk.clk,
	&pll2_pfd1_clk.clk,
	&pll2_pfd2_clk.clk,
	&pll2_pfd3_clk.clk,
	&pll3_div2_clk.clk,
	/* CCM clocks */
	&lpuart1_sel_clk.clk,
	&lpuart1_clk.clk,
	&usdhc1_sel_clk.clk,
	&usdhc1_clk.clk,
	&gpt1_sel_clk.clk,
	&gpt1_clk.clk,
	&semc_sel_clk.clk,
	&semc_clk.clk,
}

static int imxrt1170_clk_probe(struct udevice *dev)
{
	void *base;
	struct clk_bulk clk_bulk;
	int ret;

	/* add the base offset to clocks which expect an absolute address */

	/* Anatop clocks */
	base = (void *)ofnode_get_addr(ofnode_by_compatible(ofnode_null(), "fsl,imxrt-anatop"));

	pll_arm.reg += base,
	pll3_sys.reg += base,
	pll2_sys.reg += base,
	pll3_pfd0_clk.reg += base,
	pll3_pfd1_clk.reg += base,
	pll3_pfd2_clk.reg += base,
	pll3_pfd3_clk.reg += base,
	pll2_pfd0_clk.reg += base,
	pll2_pfd1_clk.reg += base,
	pll2_pfd2_clk.reg += base,
	pll2_pfd3_clk.reg += base,

	/* CCM clocks */
	base = dev_read_addr_ptr(dev);
	if (base == (void *)FDT_ADDR_T_NONE)
		return -EINVAL;

	lpuart1_sel_clk.reg += base,
	lpuart1_clk.reg += base,
	usdhc1_sel_clk.reg += base,
	usdhc1_clk.reg += base,
	gpt1_sel_clk.reg += base,
	gpt1_clk.reg += base,
	semc_sel_clk.reg += base,
	semc_clk.reg += base,

	static struct clk_array clks = {
		.arr = imxrt1170_clks,
		.count = ARRAY_SIZE(imxrt1170_clks),
	};

	ret = clk_register(dev, &clks);
	if(ret)
		return ret;

	ret = clk_get_bulk(dev, &clk_bulk);
	if (ret)
		return ret;

	clk_enable_bulk(&clk_bulk);

	return 0;
}

static const struct udevice_id imxrt1170_clk_ids[] = {
	{ .compatible = "fsl,imxrt1170-ccm" },
	{ },
};

U_BOOT_DRIVER(imxrt1170_clk) = {
	.name = "clk_imxrt1170",
	.id = UCLASS_CLK,
	.of_match = imxrt1170_clk_ids,
	.ops = &imxrt1170_clk_ops,
	.probe = imxrt1170_clk_probe,
	.flags = DM_FLAG_PRE_RELOC,
};
